info face="NeuropolXFree" size=64 bold=0 italic=0 charset="" unicode=0 stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=10,10
common lineHeight=69 base=38 scaleW=1024 scaleH=512 pages=1 packed=0
page id=0 file="neuropol_solid.png"
chars count=69
char id=124 x=10 y=10 width=24 height=69 xoffset=6 yoffset=11 xadvance=21 page=0 chnl=0 letter="|"
char id=94 x=44 y=10 width=37 height=63 xoffset=2 yoffset=6 xadvance=30 page=0 chnl=0 letter="^"
char id=40 x=91 y=10 width=29 height=62 xoffset=5 yoffset=11 xadvance=25 page=0 chnl=0 letter="("
char id=41 x=130 y=10 width=29 height=62 xoffset=4 yoffset=11 xadvance=25 page=0 chnl=0 letter=")"
char id=93 x=169 y=10 width=29 height=62 xoffset=1 yoffset=11 xadvance=22 page=0 chnl=0 letter="]"
char id=123 x=208 y=10 width=27 height=62 xoffset=1 yoffset=11 xadvance=19 page=0 chnl=0 letter="{"
char id=125 x=245 y=10 width=27 height=62 xoffset=1 yoffset=11 xadvance=19 page=0 chnl=0 letter="}"
char id=91 x=282 y=10 width=24 height=62 xoffset=6 yoffset=11 xadvance=21 page=0 chnl=0 letter="["
char id=36 x=316 y=10 width=61 height=61 xoffset=3 yoffset=12 xadvance=55 page=0 chnl=0 letter="$"
char id=96 x=387 y=10 width=24 height=60 xoffset=-0 yoffset=9 xadvance=16 page=0 chnl=0 letter="`"
char id=81 x=421 y=10 width=69 height=59 xoffset=5 yoffset=15 xadvance=66 page=0 chnl=0 letter="Q"
char id=42 x=500 y=10 width=42 height=57 xoffset=3 yoffset=12 xadvance=36 page=0 chnl=0 letter="*"
char id=37 x=552 y=10 width=76 height=56 xoffset=3 yoffset=13 xadvance=70 page=0 chnl=0 letter="%"
char id=47 x=638 y=10 width=58 height=55 xoffset=2 yoffset=14 xadvance=51 page=0 chnl=0 letter="/"
char id=92 x=706 y=10 width=58 height=55 xoffset=2 yoffset=14 xadvance=51 page=0 chnl=0 letter="\"
char id=87 x=774 y=10 width=93 height=54 xoffset=2 yoffset=15 xadvance=87 page=0 chnl=0 letter="W"
char id=77 x=877 y=10 width=88 height=54 xoffset=5 yoffset=15 xadvance=85 page=0 chnl=0 letter="M"
char id=52 x=10 y=89 width=74 height=54 xoffset=3 yoffset=15 xadvance=69 page=0 chnl=0 letter="4"
char id=65 x=94 y=89 width=73 height=54 xoffset=2 yoffset=15 xadvance=66 page=0 chnl=0 letter="A"
char id=86 x=177 y=89 width=73 height=54 xoffset=2 yoffset=15 xadvance=66 page=0 chnl=0 letter="V"
char id=38 x=260 y=89 width=71 height=54 xoffset=4 yoffset=15 xadvance=67 page=0 chnl=0 letter="&"
char id=72 x=341 y=89 width=70 height=54 xoffset=6 yoffset=15 xadvance=67 page=0 chnl=0 letter="H"
char id=78 x=421 y=89 width=69 height=54 xoffset=5 yoffset=15 xadvance=66 page=0 chnl=0 letter="N"
char id=85 x=500 y=89 width=69 height=54 xoffset=5 yoffset=15 xadvance=65 page=0 chnl=0 letter="U"
char id=80 x=579 y=89 width=65 height=54 xoffset=5 yoffset=15 xadvance=62 page=0 chnl=0 letter="P"
char id=82 x=654 y=89 width=65 height=54 xoffset=5 yoffset=15 xadvance=62 page=0 chnl=0 letter="R"
char id=88 x=729 y=89 width=65 height=54 xoffset=2 yoffset=15 xadvance=59 page=0 chnl=0 letter="X"
char id=89 x=804 y=89 width=63 height=54 xoffset=1 yoffset=15 xadvance=56 page=0 chnl=0 letter="Y"
char id=75 x=877 y=89 width=60 height=54 xoffset=5 yoffset=15 xadvance=57 page=0 chnl=0 letter="K"
char id=76 x=947 y=89 width=59 height=54 xoffset=5 yoffset=15 xadvance=55 page=0 chnl=0 letter="L"
char id=70 x=10 y=153 width=58 height=54 xoffset=5 yoffset=15 xadvance=55 page=0 chnl=0 letter="F"
char id=84 x=78 y=153 width=58 height=54 xoffset=-0 yoffset=15 xadvance=50 page=0 chnl=0 letter="T"
char id=74 x=146 y=153 width=39 height=54 xoffset=-1 yoffset=15 xadvance=30 page=0 chnl=0 letter="J"
char id=34 x=195 y=153 width=32 height=54 xoffset=6 yoffset=15 xadvance=29 page=0 chnl=0 letter="""
char id=33 x=237 y=153 width=24 height=54 xoffset=5 yoffset=15 xadvance=21 page=0 chnl=0 letter="!"
char id=73 x=271 y=153 width=23 height=54 xoffset=5 yoffset=15 xadvance=19 page=0 chnl=0 letter="I"
char id=39 x=304 y=153 width=23 height=54 xoffset=6 yoffset=15 xadvance=20 page=0 chnl=0 letter="'"
char id=49 x=337 y=153 width=22 height=54 xoffset=5 yoffset=15 xadvance=19 page=0 chnl=0 letter="1"
char id=53 x=369 y=153 width=72 height=53 xoffset=3 yoffset=16 xadvance=66 page=0 chnl=0 letter="5"
char id=56 x=451 y=153 width=72 height=53 xoffset=5 yoffset=16 xadvance=69 page=0 chnl=0 letter="8"
char id=50 x=533 y=153 width=71 height=53 xoffset=5 yoffset=16 xadvance=68 page=0 chnl=0 letter="2"
char id=51 x=614 y=153 width=71 height=53 xoffset=-1 yoffset=16 xadvance=62 page=0 chnl=0 letter="3"
char id=54 x=695 y=153 width=71 height=53 xoffset=5 yoffset=16 xadvance=68 page=0 chnl=0 letter="6"
char id=66 x=776 y=153 width=69 height=53 xoffset=5 yoffset=16 xadvance=66 page=0 chnl=0 letter="B"
char id=68 x=855 y=153 width=69 height=53 xoffset=5 yoffset=16 xadvance=66 page=0 chnl=0 letter="D"
char id=57 x=934 y=153 width=69 height=53 xoffset=3 yoffset=16 xadvance=64 page=0 chnl=0 letter="9"
char id=79 x=10 y=217 width=68 height=53 xoffset=5 yoffset=16 xadvance=64 page=0 chnl=0 letter="O"
char id=48 x=88 y=217 width=68 height=53 xoffset=5 yoffset=16 xadvance=64 page=0 chnl=0 letter="0"
char id=71 x=166 y=217 width=67 height=53 xoffset=5 yoffset=16 xadvance=64 page=0 chnl=0 letter="G"
char id=55 x=243 y=217 width=66 height=53 xoffset=-0 yoffset=16 xadvance=58 page=0 chnl=0 letter="7"
char id=83 x=319 y=217 width=63 height=53 xoffset=3 yoffset=16 xadvance=57 page=0 chnl=0 letter="S"
char id=69 x=392 y=217 width=61 height=53 xoffset=5 yoffset=16 xadvance=58 page=0 chnl=0 letter="E"
char id=90 x=463 y=217 width=61 height=53 xoffset=2 yoffset=16 xadvance=55 page=0 chnl=0 letter="Z"
char id=64 x=534 y=217 width=61 height=53 xoffset=5 yoffset=16 xadvance=57 page=0 chnl=0 letter="@"
char id=35 x=605 y=217 width=61 height=53 xoffset=2 yoffset=16 xadvance=54 page=0 chnl=0 letter="#"
char id=67 x=676 y=217 width=59 height=53 xoffset=5 yoffset=16 xadvance=56 page=0 chnl=0 letter="C"
char id=63 x=745 y=217 width=50 height=53 xoffset=5 yoffset=16 xadvance=47 page=0 chnl=0 letter="?"
char id=126 x=805 y=217 width=57 height=47 xoffset=2 yoffset=22 xadvance=50 page=0 chnl=0 letter="~"
char id=60 x=872 y=217 width=42 height=41 xoffset=-1 yoffset=28 xadvance=33 page=0 chnl=0 letter="<"
char id=62 x=924 y=217 width=38 height=41 xoffset=3 yoffset=28 xadvance=33 page=0 chnl=0 letter=">"
char id=43 x=972 y=217 width=35 height=40 xoffset=0 yoffset=29 xadvance=27 page=0 chnl=0 letter="+"
char id=59 x=10 y=280 width=22 height=40 xoffset=2 yoffset=35 xadvance=15 page=0 chnl=0 letter=";"
char id=61 x=42 y=280 width=45 height=37 xoffset=6 yoffset=32 xadvance=42 page=0 chnl=0 letter="="
char id=58 x=97 y=280 width=24 height=33 xoffset=5 yoffset=36 xadvance=21 page=0 chnl=0 letter=":"
char id=45 x=131 y=280 width=45 height=30 xoffset=6 yoffset=39 xadvance=42 page=0 chnl=0 letter="-"
char id=44 x=186 y=280 width=21 height=25 xoffset=2 yoffset=50 xadvance=14 page=0 chnl=0 letter=","
char id=95 x=217 y=280 width=70 height=20 xoffset=6 yoffset=60 xadvance=67 page=0 chnl=0 letter="_"
char id=46 x=297 y=280 width=24 height=15 xoffset=5 yoffset=54 xadvance=20 page=0 chnl=0 letter="."
char id=32 x=331 y=280 width=0 height=0 xoffset=25 yoffset=69 xadvance=25 page=0 chnl=0 letter="space"
kernings count=46
kerning first=87 second=65 amount=-10
kerning first=87 second=79 amount=-4
kerning first=87 second=48 amount=-4
kerning first=87 second=83 amount=-2
kerning first=65 second=87 amount=-10
kerning first=65 second=86 amount=-10
kerning first=65 second=89 amount=-10
kerning first=65 second=76 amount=-5
kerning first=65 second=84 amount=-8
kerning first=86 second=65 amount=-10
kerning first=86 second=79 amount=-4
kerning first=86 second=48 amount=-4
kerning first=86 second=83 amount=-2
kerning first=89 second=65 amount=-10
kerning first=89 second=79 amount=-4
kerning first=89 second=48 amount=-4
kerning first=89 second=83 amount=-2
kerning first=76 second=87 amount=-11
kerning first=76 second=65 amount=3
kerning first=76 second=86 amount=-11
kerning first=76 second=89 amount=-11
kerning first=76 second=34 amount=-5
kerning first=76 second=39 amount=-5
kerning first=76 second=79 amount=-2
kerning first=76 second=48 amount=-2
kerning first=84 second=65 amount=-8
kerning first=34 second=83 amount=-2
kerning first=39 second=83 amount=-2
kerning first=79 second=87 amount=-4
kerning first=79 second=86 amount=-4
kerning first=79 second=89 amount=-4
kerning first=79 second=34 amount=-4
kerning first=79 second=39 amount=-4
kerning first=48 second=87 amount=-4
kerning first=48 second=86 amount=-4
kerning first=48 second=89 amount=-4
kerning first=48 second=34 amount=-4
kerning first=48 second=39 amount=-4
kerning first=83 second=87 amount=-7
kerning first=83 second=86 amount=-7
kerning first=83 second=89 amount=-7
kerning first=83 second=84 amount=-9
kerning first=69 second=87 amount=-7
kerning first=69 second=86 amount=-7
kerning first=69 second=89 amount=-7
kerning first=69 second=84 amount=-9
