using UnityEngine;
using System.Collections;

/// <summary>
/// Visual feedback tools. Convenience for creating feedback objects for the user to see
/// </summary>
public class VisualFeedbackHandler : MonoBehaviour{
	
	public Transform feedbackUIPanel;
	public GameObject feedbackPrefab;
	
	private static VisualFeedbackHandler instance;
	public static VisualFeedbackHandler Instance {
		get { 
			if(instance == null) 
				Debug.Log("VisualFeedbackHandler has not been placed in the scene");
			return instance;
		}
	}
	
	void Awake()
	{
		instance = this;
	}
		
	public static StatusFeedback SpawnFeedback(FeedbackInformation info, Vector3 pos) {
				
		Transform t =  PoolManager.Pools["UI"].Spawn(VisualFeedbackHandler.instance.feedbackPrefab.transform);

		//UICamera cam = UICamera.FindCameraForLayer(Layers.UILayer);
		
		t.parent = VisualFeedbackHandler.instance.feedbackUIPanel;
		
		t.localPosition = Vector3.zero;
		
		t.localRotation = Quaternion.identity;
		
		t.localScale = VisualFeedbackHandler.instance.feedbackPrefab.transform.localScale;
		
		StatusFeedback statusFeedback = t.GetComponent<StatusFeedback>();
		
		statusFeedback.BeginFeedback(info, pos);
		
		return statusFeedback;
	}
	
	public static StatusFeedback SpawnFeedback(FeedbackInformation info, Transform target) {
				
		Transform t =  PoolManager.Pools["UI"].Spawn(VisualFeedbackHandler.instance.feedbackPrefab.transform);
		
		t.parent = VisualFeedbackHandler.instance.feedbackUIPanel;
		
		t.localPosition = Vector3.zero;
		
		t.localRotation = Quaternion.identity;
		
		t.localScale = VisualFeedbackHandler.instance.feedbackPrefab.transform.localScale;
		
		StatusFeedback statusFeedback = t.GetComponent<StatusFeedback>();
		
		statusFeedback.BeginFeedback(info, target);
		
		return statusFeedback;
	}	
}
