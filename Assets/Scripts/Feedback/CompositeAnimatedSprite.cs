using UnityEngine;
using System.Collections;

public class CompositeAnimatedSprite : MonoBehaviour {

	public tk2dAnimatedSprite[] animatedSprites;
	public string prefabPool;
	int spriteCount;
	
	void Awake()
	{
		spriteCount = 0;
		
		foreach(tk2dAnimatedSprite animatedSprite in animatedSprites) {
			
			animatedSprite.animationCompleteDelegate += OnCompleteDelegate;
			
		}
	}
	
	void OnSpawned()
	{
		foreach(tk2dAnimatedSprite animatedSprite in animatedSprites) {
			
			animatedSprite.Play();
			
		}		
	}

	void OnCompleteDelegate(tk2dAnimatedSprite sprite, int clipId) 
	{
		//this.GetComponent<tk2dSprite>().scale = Vector3.one;
		spriteCount ++;
		if(spriteCount == animatedSprites.Length) {
			if(prefabPool.Length > 0)
				PoolManager.Pools[prefabPool].Despawn(this.transform);
			else 
				Destroy(this.gameObject);
		}
	}
	
	void OnDespawned()
	{
		spriteCount = 0;
	}
	
}