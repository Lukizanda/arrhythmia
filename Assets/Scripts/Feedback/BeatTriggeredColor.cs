using UnityEngine;
using System.Collections;

public class BeatTriggeredColor : MonoBehaviour {
	
	public Color originalColor;
	public Color brightenedColor;
	bool tweenColorRunning;

	void Start()
	{
		// register for events we're interest in
		if(BeatManager.Shared != null) {
			ReceiverItem beatChangeListener = new ReceiverItem(this.gameObject, "OnBeat", 0.0f);
			BeatManager.Shared.beatSignalSender.receivers.Add(beatChangeListener);		
		}
	}
	
	void OnBeat ()
	{
		if(!tweenColorRunning) {
			TweenColor colorTween = TweenColor.Begin(this.gameObject, .1f, brightenedColor);
			colorTween.callWhenFinished = "OnColorBrightenComplete";
			colorTween.eventReceiver = this.gameObject;
			tweenColorRunning = true;
		}		
		
	}
	
	void OnColorBrightenComplete()
	{
		TweenColor colorTween = TweenColor.Begin(this.gameObject, .3f, originalColor);
		colorTween.method = UITweener.Method.EaseOut;
		colorTween.callWhenFinished = "OnColorDarkenComplete";
		colorTween.eventReceiver = this.gameObject;
	}
	
	void OnColorDarkenComplete()
	{

		tweenColorRunning = false;

	}
}
