using UnityEngine;
using System.Collections;

public class FadeSprite : MonoBehaviour {

	public float duration = 0;
	
	tk2dSprite objectSprite;
	Color originalColor;
	Color alphaColor;
	float startTime; 
		
	/// <summary>
	/// Start this instance. Initialize the need values;
	/// </summary> 
	void Awake()
	{
		objectSprite = this.GetComponent<tk2dSprite>();
		originalColor = objectSprite.color;
		alphaColor = new Color(originalColor.r, originalColor.g, originalColor.b, 0.0f);
	}
	
	void Update()
	{
		//if(this.renderer.enabled == false) this.renderer.enabled = true;
		
		if(Time.time - startTime < duration) {
			
			objectSprite.color = Color.Lerp(alphaColor, originalColor, Mathf.Clamp01 (0 + (Time.time - startTime)/duration));

		} else {
			
			this.enabled = false;
			
		}
	}
	
	
	/// <summary>
	/// Begin fade on the specified go
	/// </summary>
	/// <param name='go'>
	/// The game object affected
	/// </param>
	/// <param name='duration'>
	/// Duration.
	/// </param>

	static public void Begin(GameObject go, float duration) 
	{							
		/// Recurse through object and make intangible what can be intangible	
		if(go.GetComponent<tk2dSprite>() != null) {
				
			FadeSprite fadeSprite = go.GetComponent<FadeSprite>();
			
			if(fadeSprite == null) fadeSprite = go.AddComponent<FadeSprite>();
						
			fadeSprite.duration = duration;
			
			fadeSprite.startTime = Time.time;
			
			//fadeSprite.renderer.enabled = false;
			
			if(fadeSprite.enabled ==false) fadeSprite.enabled = true;
			
			fadeSprite.GetComponent<tk2dSprite>().color = Color.clear;
			
		}
		
		foreach(Transform child in go.transform) {
			
			if(child.gameObject.active)
				FadeSprite.Begin(child.gameObject, duration);
			
		}

		
	
	}
	
}

