using UnityEngine;
using System.Collections;

/// <summary>
/// Visual feedback tools. Convenience for creating feedback objects for the user to see
/// </summary>
static public class VisualFeedbackTools  {
	
	public static StatusFeedback SpawnFeedback(FeedbackInformation info, Vector3 pos) {
		
		GameObject feedbackPrefab = Resources.Load("Visual Feedback/Status Feedback", typeof(GameObject)) as GameObject;
		
		Transform t =  PoolManager.Pools["UI"].Spawn(feedbackPrefab.transform);

		UICamera cam = UICamera.FindCameraForLayer(Layers.UILayer);
		
		t.parent = cam.gameObject.transform;	
		
		t.localPosition = Vector3.zero;
		
		t.localRotation = Quaternion.identity;
		
		t.localScale = feedbackPrefab.transform.localScale;
		
		StatusFeedback statusFeedback = t.GetComponent<StatusFeedback>();
		
		statusFeedback.BeginFeedback(info, pos);
		
		return statusFeedback;
	}
	
	public static StatusFeedback SpawnFeedback(FeedbackInformation info, Transform target) {
		
		GameObject feedbackPrefab = Resources.Load("Visual Feedback/Status Feedback", typeof(GameObject)) as GameObject;
		
		Transform t =  PoolManager.Pools["UI"].Spawn(feedbackPrefab.transform);

		UICamera cam = UICamera.FindCameraForLayer(Layers.UILayer);
		
		t.parent = cam.gameObject.transform;	
		
		t.localPosition = Vector3.zero;
		
		t.localRotation = Quaternion.identity;
		
		t.localScale = feedbackPrefab.transform.localScale;
		
		StatusFeedback statusFeedback = t.GetComponent<StatusFeedback>();
		
		statusFeedback.BeginFeedback(info, target);
		
		return statusFeedback;
	}	
}
