using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class AudioFeedback : MonoBehaviour {
	
	AudioSource mAudio;
	public bool isFIFO = true;
	
	// Use this for initialization
	IEnumerator OnSpawned () {
	
		mAudio = this.GetComponent<AudioSource>();
		mAudio.Play();
		
		if(!isFIFO) {
			while(mAudio.isPlaying)
				yield return null;
			
			PoolManager.Pools["Audio"].Despawn(this.transform);			
		}
		
		yield return null;


	}

}
