using UnityEngine;
using System.Collections;

//[RequireComponent(typeof(TweenColor))]
//[RequireComponent(typeof(UILabel))]
public class StatusFeedback : MonoBehaviour {
	
	public UILabel label;
	public UILabel labelGlow;
	public bool glowEnabled = true;
	//TweenColor colorTweener;
	public float duration = 1.5f;
	Vector3 mPos;
	float yOffset = 0;
	Transform target;
	
	public void BeginFeedback(FeedbackInformation feedbackInfo, Vector3 pos) {
		
		// store the pos in a local variable in case we want to modify it later in the coroutine
		this.mPos = pos;
				
		label.text = feedbackInfo.DisplayString;
		
		label.color = feedbackInfo.LabelColor;

		label.transform.localScale = new Vector3(feedbackInfo.FontSize, feedbackInfo.FontSize, 1.0f);
		
		if(glowEnabled)	{

			labelGlow.gameObject.SetActiveRecursively (true);

			labelGlow.text = feedbackInfo.DisplayString;
			
			labelGlow.color = Color.white;
			
			labelGlow.transform.localScale = new Vector3(feedbackInfo.FontSize, feedbackInfo.FontSize, 1.0f);
		
		} else { 
			if(labelGlow != null)
				labelGlow.gameObject.SetActiveRecursively (false);
		}
		
		this.duration = feedbackInfo.Duration;
		
		if(feedbackInfo.YOffset > 0) {
			this.yOffset = feedbackInfo.YOffset;
		}
				
		// Position the feedback object
		mPos = AnchorManager.Shared.mainCamera.WorldToViewportPoint(mPos);		
		mPos = AnchorManager.Shared.uiCamera.camera.ViewportToWorldPoint(mPos);
				
		// set position of label
		transform.position = new Vector3(mPos.x, mPos.y);
		
		// now we can check in local scale, coz if we did this step earlier, the coordinates would be messed up because we were in main camera dimensions i think
		// now we tweak in UICamera coordinates using label local position
		if(this.yOffset > 0)
			transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y + this.yOffset, -5f);	
		
		transform.localPosition = this.CheckOffscreen(transform.localPosition);
		
		this.StartCoroutine("FixedPositionFeedbackCoroutine");
				
	}
	
	public void BeginFeedback(FeedbackInformation feedbackInfo, Transform target) {
		
		label.text = feedbackInfo.DisplayString;
		
		label.color = feedbackInfo.LabelColor;

		label.transform.localScale = new Vector3(feedbackInfo.FontSize, feedbackInfo.FontSize, 1.0f);
		
		if(glowEnabled) {
		
			labelGlow.gameObject.SetActiveRecursively(true);
			
			labelGlow.text = feedbackInfo.DisplayString;
		
			labelGlow.color = Color.white;
			
			labelGlow.transform.localScale = new Vector3(feedbackInfo.FontSize, feedbackInfo.FontSize, 1.0f);
		
		} else {
			
			labelGlow.gameObject.SetActiveRecursively(false);
			
		}
		
		this.target = target;
		
		this.duration = feedbackInfo.Duration;

		this.StartCoroutine("FeedbackCoroutine");
		
	}
	
	
	public void BeginFeedback(string dislayString, Color color, Transform target, float duration)
	{
		label.text = dislayString;
		
		if(duration > 0)
			this.duration = duration;
				
		this.StartCoroutine("FeedbackCoroutine");
	}
	
	IEnumerator FixedPositionFeedbackCoroutine()
	{				
		
		iTween.ScaleFrom(gameObject, new Vector3( transform.localScale.x * 1.2f,  transform.localScale.y * 1.2f, 1.0f), 1f);

		yield return new WaitForSeconds(duration);
				
		//TweenColor.Begin(this.gameObject, .2f, Color.clear);
		TweenColor.Begin(label.gameObject, .2f, Color.clear);
		if(labelGlow!= null)
			TweenColor.Begin(labelGlow.gameObject, .2f, Color.clear);
		
		yield return new WaitForSeconds(.5f);
		
		this.Reset();		
	}
	
	IEnumerator FeedbackCoroutine()
	{
		float startTime = Time.time;
		
		PositionFeedbackAboveTarget();

		iTween.ScaleFrom(gameObject, new Vector3( transform.localScale.x * 1.2f,  transform.localScale.y * 1.2f), .5f);		

		while(Time.time - startTime < duration && target != null) {
						
			if(target.gameObject.active == false)
				break;
			
			yield return null;
		}

		//TweenColor.Begin(this.gameObject, .2f, Color.clear);
		TweenColor.Begin(label.gameObject, .2f, Color.clear);
		TweenColor.Begin(labelGlow.gameObject, .2f, Color.clear);
		
		yield return new WaitForSeconds(.5f);		
		
		this.Reset();
	}
	
	void PositionFeedbackAboveTarget()
	{
		mPos = AnchorManager.Shared.mainCamera.WorldToViewportPoint(target.position);
		
		mPos = AnchorManager.Shared.uiCamera.camera.ViewportToWorldPoint(mPos);

		transform.position = new Vector3(mPos.x, mPos.y);			
		
		if(this.yOffset > 0)
			transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y + this.yOffset, -5f);	
		
		transform.localPosition = this.CheckOffscreen(transform.localPosition);
		//if(this.yOffset > 0) {
		//	transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y + yOffset, -5.0f);		
		//}
	}
	
	void OnDespawn()
	{
		yOffset = 0.0f;
		duration = 0.0f;
	}
	
	void Reset()
	{
		if(target)
			target.SendMessage("FeedbackComplete", this.gameObject, SendMessageOptions.DontRequireReceiver);
		
		PoolManager.Pools["UI"].Despawn(this.transform);
		
	}
	
	
	Vector3 CheckOffscreen(Vector3 screenPosition)
	{
		// Get the bounds of the widget (UILabel)
		Bounds labelBounds = NGUIMath.CalculateRelativeWidgetBounds(label.transform);
		
		// we need to times by the scale of the text; divide by 2 because the UI for the feedback is anchored in the center
		// we need to divide the width by 2 because the text is center aligned
		int pixelWidth =  Mathf.RoundToInt(labelBounds.size.x * label.transform.localScale.x / 2);
		int pixelHeight = Mathf.RoundToInt(labelBounds.size.y * label.transform.localScale.y);
		
		// now get the camera height and width
		int midHeight = Mathf.RoundToInt(UICamera.mainCamera.pixelHeight / 2);
		int midWidth = Mathf.RoundToInt(UICamera.mainCamera.pixelWidth / 2);
		
		// if text will appear off screen, adjust so that it falls within view
		if( screenPosition.x + pixelWidth > midWidth ) {
			
			screenPosition.x -= ((screenPosition.x + pixelWidth) - midWidth);
					
		}
		else if( screenPosition.x - pixelWidth < -midWidth ) {
			
			screenPosition.x +=  (-midWidth - (screenPosition.x - pixelWidth));
		
		}
		
		if( screenPosition.y + pixelHeight > midHeight ) {
			
			screenPosition.y = screenPosition.y - pixelHeight;
		
		}
		else if( screenPosition.y - pixelHeight < -midHeight) {
			
			screenPosition.y = screenPosition.y + pixelHeight;
		
		}
					
		return screenPosition;
	}
	
}
