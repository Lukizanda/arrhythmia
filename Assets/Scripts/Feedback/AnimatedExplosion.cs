using UnityEngine;
using System.Collections;

public class AnimatedExplosion : MonoBehaviour {
	
	tk2dAnimatedSprite anim;
	public string clipToPlay;
	public string prefabPool = "Explosion";
	public Vector3 customScale = Vector3.one;
	
	// Use this for initialization
	void Awake () {
		anim = gameObject.GetComponent<tk2dAnimatedSprite>();
		anim.animationCompleteDelegate = OnCompleteDelegate;
	}

	void OnSpawned()
	{
		this.GetComponent<tk2dSprite>().scale = customScale;
		anim.Play(clipToPlay);
	}

	void OnCompleteDelegate(tk2dAnimatedSprite sprite, int clipId) 
	{
		this.GetComponent<tk2dSprite>().scale = Vector3.one;
		if(prefabPool.Length > 0)
			PoolManager.Pools[prefabPool].Despawn(this.transform);
		else
			DestroyObject(this.gameObject);
	}
	
}
