using UnityEngine;
using System.Collections;

public class BeatTriggeredScale : MonoBehaviour {
	
	public Vector3 initialScale = Vector3.one;
	public Vector3 finalScale = new Vector3(1.5f, 1.5f, 1);
	
	// Use this for initialization
	void Start () {
		
		ReceiverItem beatListener = new ReceiverItem(this.gameObject, "OnBeat", 0.0f);
		BeatManager.Shared.beatSignalSender.receivers.Add(beatListener);
		
	}
	
	void OnBeat()
	{
		iTween.PunchScale(this.gameObject, finalScale, .5f);
	}
	
}
