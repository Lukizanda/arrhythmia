using UnityEngine;
using System.Collections;

public class FreeMovementMotor : MovementMotor {
	
	public float speed = 5.0f;
	public float snappiness = 50.0f;
	
	// Update is called once per frame
	void FixedUpdate () {
		
		Vector3 targetVelocity = movementDirection * speed;
		
		Vector3 deltaVelocity = targetVelocity - rigidbody.velocity;
		
		if(rigidbody.useGravity)
			deltaVelocity.y = 0;
		
		rigidbody.AddForce(deltaVelocity * snappiness, ForceMode.Acceleration);
	
		// Setup player to face facingDirection, or if that is zero, then the movementDirection
		Vector3 faceDir = facingDirection;
		if(faceDir == Vector3.zero)
			faceDir = movementDirection;
		
		if(faceDir == Vector3.zero) {
			
			rigidbody.angularVelocity = Vector3.zero;

		} else {
			
			float rotationAngle = AngleTools.AngleAroundAxis(Vector3.up, faceDir, Vector3.forward);
			transform.rotation = Quaternion.Euler(Vector3.forward * rotationAngle);
			
		}
		
	}
}
