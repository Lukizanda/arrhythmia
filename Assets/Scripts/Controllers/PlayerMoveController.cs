using UnityEngine;
using System.Collections;

public class PlayerMoveController : MonoBehaviour {
	
	private MovementMotor motor;
	
	void Awake () 
	{
		motor = this.GetComponent<MovementMotor>();
	}
	
	// Update is called once per frame
	void Update () {
	
		motor.movementDirection = Input.GetAxis ("Horizontal") * Vector3.right + Input.GetAxis("Vertical") * Vector3.up;
		
		if(motor.movementDirection.sqrMagnitude > 1)
			motor.movementDirection.Normalize();
		
	}
}
