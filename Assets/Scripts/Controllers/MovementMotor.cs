using UnityEngine;
using System.Collections;

public class MovementMotor : MonoBehaviour {
	
	[HideInInspector]
	public Vector3 movementDirection;
	[HideInInspector]
	public Vector3 movementTarget;
	[HideInInspector]
	public Vector3 facingDirection;
	
}
