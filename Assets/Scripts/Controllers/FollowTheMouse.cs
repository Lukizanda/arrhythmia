using UnityEngine;
using System.Collections;

public class FollowTheMouse : PausableMonobehaviour {

    public float maxSpeed;
    public float moveSpeed;
    public float turnSpeed;
    public float origParticleMinSize;
    public float origParticleMaxSize;
    public float origParticleLocalYSpeed;
    public ParticleEmitter emitter;

	// Use this for initialization
	void Start () {
		
		if(Application.platform == RuntimePlatform.IPhonePlayer)
			this.enabled = false;
		
		if(emitter != null) {
        	origParticleMinSize = emitter.minSize;
        	origParticleMaxSize = emitter.maxSize;
        	origParticleLocalYSpeed = emitter.localVelocity.y;
		}
	}
	
	void FixedUpdate () {
		
		if(paused)
			return;
		
		Vector3 worldPos;
		
		if(Input.GetMouseButton(0) == false) {
			return;
		}
		
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 1.0f;
        worldPos = Camera.main.ScreenToWorldPoint(mousePos);
		
        Vector3 dirVec = worldPos - transform.position;
		
		Vector3 targetVelocity = dirVec * moveSpeed;
		
		Vector3 deltaVelocity = targetVelocity - rigidbody.velocity;

        if (dirVec.magnitude > 1f)
        {
            rigidbody.AddForce(deltaVelocity, ForceMode.Acceleration);
			
            if (Mathf.Abs(Vector3.Dot(transform.right * -1f, dirVec.normalized)) > 0.01f)
            {
				float rotationAngle = AngleTools.AngleAroundAxis(Vector3.up, dirVec, Vector3.forward);
				//rigidbody.angularVelocity = (Vector3.right * rotationAngle);		
				transform.rotation = Quaternion.Euler(Vector3.forward * rotationAngle);
				//float rotationAngle = AngleTools.AngleAroundAxis(Vector3.up, dirVec, Vector3.forward);
				//Debug.Log(rotationAngle);
				//rigidbody.angularVelocity = (Vector3.right * rotationAngle);
                //rigidbody.angularVelocity = new Vector3(0f, 0f, Vector3.Dot(transform.right*-1f, dirVec.normalized) * turnSpeed);
            }
        }

        if (rigidbody.velocity.magnitude > maxSpeed)
        {
           rigidbody.velocity = rigidbody.velocity.normalized * maxSpeed;
        }

		if(emitter != null) {
        	emitter.minSize = origParticleMinSize * (rigidbody.velocity.magnitude / maxSpeed);
        	emitter.maxSize = origParticleMaxSize * (rigidbody.velocity.magnitude / maxSpeed);
        	emitter.localVelocity = new Vector3(0f, origParticleLocalYSpeed * (rigidbody.velocity.magnitude / maxSpeed), 0f);
		}
	}		
	
}
