using UnityEngine;
using System.Collections;

public class AnchorManager : MonoBehaviour {

	public Camera mainCamera;
	public UICamera uiCamera;
	
	private Transform leftGUIAnchor;
	public Transform LeftGUIAnchor	{
		get { 
			if(leftGUIAnchor == null) {
				// Attempt to find and assign anchor
				if(uiCamera == null) 
					uiCamera = UICamera.FindCameraForLayer(Layers.UILayer);
				
				UIAnchor[] anchors = uiCamera.GetComponentsInChildren<UIAnchor>();
				foreach(UIAnchor anchor in anchors) 
				{
					if(anchor.side == UIAnchor.Side.Left) {
						leftGUIAnchor = anchor.transform;
						break;
					}
				}
			}
			return leftGUIAnchor;
		}		
	}
	
	private Transform topGUIAnchor;
	public Transform TopGUIAnchor {
		get {
			if(topGUIAnchor == null) {
				// Attempt to find and assign anchor
				if(uiCamera == null) 
					uiCamera = UICamera.FindCameraForLayer(Layers.UILayer);
				
				UIAnchor[] anchors = uiCamera.GetComponentsInChildren<UIAnchor>();
				foreach(UIAnchor anchor in anchors) 
				{
					if(anchor.side == UIAnchor.Side.Top) {
						topGUIAnchor = anchor.transform;
						break;
					}
				}
			}
			return topGUIAnchor;
		}
	}
	
	private Transform bottomLeftGUIAnchor;
	public Transform BottomLeftGUIAnchor {
		get { 
			if(bottomLeftGUIAnchor == null) {
				// Attempt to find and assign anchor
				if(uiCamera == null) 
					uiCamera = UICamera.FindCameraForLayer(Layers.UILayer);
				
				UIAnchor[] anchors = uiCamera.GetComponentsInChildren<UIAnchor>();
				foreach(UIAnchor anchor in anchors) 
				{
					if(anchor.side == UIAnchor.Side.BottomLeft) {
						bottomLeftGUIAnchor = anchor.transform;
						break;
					}
				}
			}
			return bottomLeftGUIAnchor;
		}
	}
	
	private Transform bottomRightGUIAnchor;
	public Transform BottomRightGUIAnchor {
		get { 
			if(bottomRightGUIAnchor == null) {
				// Attempt to find and assign anchor
				if(uiCamera == null) 
					uiCamera = UICamera.FindCameraForLayer(Layers.UILayer);
				
				UIAnchor[] anchors = uiCamera.GetComponentsInChildren<UIAnchor>();
				foreach(UIAnchor anchor in anchors) 
				{
					if(anchor.side == UIAnchor.Side.BottomRight) {
						bottomRightGUIAnchor = anchor.transform;
						break;
					}
				}
			}
			return bottomRightGUIAnchor;
		}
	}

	private Transform centerGUIAnchor;
	public Transform CenterGUIAnchor {
		get { 
			if(centerGUIAnchor == null) {
				// Attempt to find and assign anchor
				if(uiCamera == null) {
					uiCamera = UICamera.FindCameraForLayer(Layers.UILayer);
				}
				UIAnchor[] anchors = uiCamera.GetComponentsInChildren<UIAnchor>();
				foreach(UIAnchor anchor in anchors) 
				{
					if(anchor.side == UIAnchor.Side.Center) {
						centerGUIAnchor = anchor.transform;
						break;
					}
				}
			}
			return centerGUIAnchor;
		}
	}

	private Transform topRightGUIAnchor;
	public Transform TopRightGUIAnchor {
		get { 
			if(topRightGUIAnchor == null) {
				// Attempt to find and assign anchor
				if(uiCamera == null) {
					uiCamera = UICamera.FindCameraForLayer(Layers.UILayer);
				}
				UIAnchor[] anchors = uiCamera.GetComponentsInChildren<UIAnchor>();
				foreach(UIAnchor anchor in anchors) 
				{
					if(anchor.side == UIAnchor.Side.TopRight) {
						topRightGUIAnchor = anchor.transform;
						break;
					}
				}
			}
			return topRightGUIAnchor;
		}
	}
	
	private Transform topLeftGUIAnchor;
	public Transform TopLeftGUIAnchor {
		get { 
			if(topLeftGUIAnchor == null) {
				// Attempt to find and assign anchor
				if(uiCamera == null) {
					uiCamera = UICamera.FindCameraForLayer(Layers.UILayer);
				}
				UIAnchor[] anchors = uiCamera.GetComponentsInChildren<UIAnchor>();
				foreach(UIAnchor anchor in anchors) 
				{
					if(anchor.side == UIAnchor.Side.TopLeft) {
						topLeftGUIAnchor = anchor.transform;
						break;
					}
				}
			}
			return topLeftGUIAnchor;
		}
	}	
	
	private static AnchorManager shared;
	public static AnchorManager Shared {
		get {
			if(shared == null) {
				Debug.LogWarning("There is no anchor manager on the scene but something is trying to use it");
			}
			return shared;
		}
	}
	
	void Awake()
	{
		shared = this;
		
		mainCamera = Camera.mainCamera;

		if(uiCamera == null) 
			uiCamera = UICamera.FindCameraForLayer(Layers.UILayer);
		
		// Try to find anchors and assign
		if(uiCamera) {
			
			UIAnchor[] anchors = uiCamera.GetComponentsInChildren<UIAnchor>();
			
			foreach(UIAnchor anchor in anchors) 
			{
				if(anchor.side == UIAnchor.Side.BottomLeft)
					bottomLeftGUIAnchor = anchor.transform;
				if(anchor.side == UIAnchor.Side.Left)
					leftGUIAnchor = anchor.transform;
				if(anchor.side == UIAnchor.Side.Top)
					topGUIAnchor = anchor.transform;
				if(anchor.side == UIAnchor.Side.Center)
					centerGUIAnchor = anchor.transform;
				if(anchor.side == UIAnchor.Side.BottomRight)
					bottomRightGUIAnchor = anchor.transform;
				if(anchor.side == UIAnchor.Side.TopRight)
					topRightGUIAnchor = anchor.transform;
				if(anchor.side == UIAnchor.Side.TopLeft)
					topLeftGUIAnchor = anchor.transform;
			}
		}
		
		
	}
	
}
