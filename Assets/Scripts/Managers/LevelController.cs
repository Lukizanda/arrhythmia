using UnityEngine;
using System.Collections;

public class LevelController : PausableMonobehaviour {
	
	public GameObject playerGO;
		
	public float baseTimeLimit = 120;
	
	public float timeLimitAddedPerLevel = 10f;
	
	public bool countdownTimerIsRunning = false;
	
	// FYI JUST PUTTING THE UI STUFF HERE> MESSY BUT TIME IS SHORT
	public GameObject newLevelGroupGO;
	public UILabel newLevelLabel;
	public UILabel newGoalLabel;
	public TweenColor newLevelTween;
	public TweenColor newGoalTween;
	
	public GameObject gameOverGroupGO;
	public UILabel levelReachedLabel;
	
	public GameObject rulesGroupGO;
		
	public GameObject startIntructionsGO;
	
	public GameObject creditsPageGO;
	
	public Color newLevelLabelColor = Color.white;
	
	public GameObject flatLineAudioFeedbackGO;
	
	[HideInInspector]
	public bool gameNotStarted = true;
	public bool isPlaying = false;
	
	private float currentTimeRemaining = 0;
	public float CurrentTimeRemaining {
		get { return currentTimeRemaining; }
	}
	
	private int currentLevel = 1;
	public int Currentlevel {
		get { return currentLevel; }
	}
	
	public int baseScoreTarget = 100;
	
	
	private int currentScoreTarget = 40; 
	public int CurrentScoreTarget {
		get { return currentScoreTarget; }
	}
	
	public float levelScoreTargetIncreaseMultiplier = 1.5f;

	// Signals
	public SignalSender gameRestartedSignalSender;
	public SignalSender gameOverSignalSender;
	public SignalSender startNewLevelSignalSender;
	public SignalSender levelWinSignalSender;
	public SignalSender gameStartSignalSender;
	
	private static LevelController shared;
	public static LevelController Shared {
		get { 
			if(shared == null)
				Debug.Log("Level controller is not on the scene but something is trying to use it");
			
			return shared; 
		}
	}
	
	void Awake()
	{
		shared = this;
		
		currentScoreTarget = baseScoreTarget;
	}
	
	// Use this for initialization
	void Start () 
	{	
		ReceiverItem playerDeathReceiver = new ReceiverItem(this.gameObject, "OnPlayerDeath", 0.0f);
		Player.Shared.playerDeathSignal.receivers.Add(playerDeathReceiver);	
		
		this.StartCoroutine("StartUpCoroutine");
	}
	
	IEnumerator StartUpCoroutine(){
	
		gameNotStarted = true;
		
		startIntructionsGO.SetActiveRecursively(true);
		
		while(gameNotStarted) {
		
			if(Input.GetKey(KeyCode.KeypadEnter) || Input.GetKey(KeyCode.Return)) {
				
				gameNotStarted = false;
				
				startIntructionsGO.SetActiveRecursively(false);
				
				creditsPageGO.SetActiveRecursively(false);
				
				gameStartSignalSender.SendSignals(this);
								
			}
		
			
			yield return null;
		}
		
		this.StartCoroutine("StartNewLevelCoroutine");
		
		yield return null;
	}
	
	void OnPlayerDeath() 
	{	
		playerGO.SetActiveRecursively(false);
		
		PoolManager.Pools["Audio"].Spawn(flatLineAudioFeedbackGO.transform);
		
		this.StopCoroutine("CountdownTimerCoroutine");
		
		countdownTimerIsRunning = false;
		
		DestroyAllPickups();
		
		DestroyAllProjectiles();
		
		gameOverSignalSender.SendSignals(this);
				
		this.isPlaying = false;
				
		this.StartCoroutine("GameOverCoroutine");
	}
	
	public void WinLevel()
	{
		levelWinSignalSender.SendSignals(this);
		
		//this.StopCoroutine("CountdownTimerCoroutine");
		currentTimeRemaining += timeLimitAddedPerLevel;
		
		ScoreController.Shared.Reset();
		
		currentScoreTarget += (int)Mathf.Round(currentScoreTarget * levelScoreTargetIncreaseMultiplier/10) * 10; // Round to 10 so it feels less weird? =S
		
		currentLevel ++;
		
		DestroyAllProjectiles();
		
		this.StartCoroutine("StartNewLevelCoroutine");
	}
	
	void DestroyAllProjectiles() 
	{
		GameObject[] bulletGOs = GameObject.FindGameObjectsWithTag("Projectile");
		foreach(GameObject go in bulletGOs)
			go.SendMessage("DestroySelf");
	}
	
	void DestroyAllPickups()
	{
		GameObject[] pickupGOs = GameObject.FindGameObjectsWithTag("Pickup");
		foreach(GameObject go in pickupGOs)
		go.SendMessage("DestroySelf");	
	}
	
	void Reset()
	{
		currentLevel = 1;
		
		currentScoreTarget = baseScoreTarget;			
		
		currentTimeRemaining = baseTimeLimit;		
	}
	
	void DisplayCreditsPage()
	{
		creditsPageGO.SetActiveRecursively(true);
	}
	
	void HideCreditsPage()
	{
		creditsPageGO.SetActiveRecursively(false);
	}
	
	void DisplayRulesPage()
	{
		rulesGroupGO.SetActiveRecursively(true);
	}
	
	void HideRulesPage()
	{
		rulesGroupGO.SetActiveRecursively(false);
	}
	
	IEnumerator GameOverCoroutine()
	{
		gameNotStarted = true;
		
		yield return new WaitForSeconds(1f);
		
		gameOverGroupGO.SetActiveRecursively(true);
		
		if(currentLevel > 1) {
			levelReachedLabel.text = "[FF5A00]YOU REACHED \n\n[FFFFFF]LEVEL "+currentLevel;//+"\n\n\nNICE";
			
		} else {
			levelReachedLabel.text = "[FF5A00]Oops\n\nBetter luck next time.";//+"\n\n\nOops. \n\nBetter luck next time";
		}
		
		while(gameNotStarted) {
		
			if(Input.GetKey(KeyCode.KeypadEnter) || Input.GetKey(KeyCode.Return)) {
				
				gameNotStarted = false;
				
				gameOverGroupGO.SetActiveRecursively(false);
				
				rulesGroupGO.SetActiveRecursively(false);
				
				gameRestartedSignalSender.SendSignals(this);
				
				//FYI HACK
				BeatManager.Shared.beatSlider.sliderValue = .5f;
				ScoreUI.Shared.Reset();
				
				this.Reset();

			}
		
			
			yield return null;
		}
		
		
		this.StartCoroutine("StartNewLevelCoroutine");
		
		yield return null;		
	}
	
	IEnumerator StartNewLevelCoroutine()
	{		
		newLevelGroupGO.SetActiveRecursively(true);
		newLevelLabel.text = "LEVEL " + currentLevel;
		newGoalLabel.text = "[FF5A00]TO REACH THE NEXT LEVEL \n\nGET [FFFFFF]" + currentScoreTarget+" [FF5A00]POINTS";
		
		playerGO.SetActiveRecursively(true);		
		
		// setup tween values
		newGoalTween.from = Color.clear;
		newGoalTween.to = newLevelLabelColor;
		newGoalTween.duration = .5f;
		
		newLevelTween.from = Color.clear;
		newLevelTween.to = Color.white;
		newLevelTween.duration = .5f;
				
		newGoalTween.Reset();
		newLevelTween.Reset();
		
		newGoalTween.Play(true);
		newLevelTween.Play(true);
		
		yield return new WaitForSeconds(2f);
		
		// setup tween values
		newGoalTween.from = newLevelLabelColor;
		newGoalTween.to = Color.clear;
		newGoalTween.duration = .5f;
		
		newLevelTween.from =  Color.white;
		newLevelTween.to = Color.clear;
		newLevelTween.duration = .5f;
		
		newGoalTween.Reset();
		newLevelTween.Reset();
		newGoalTween.Play(true);
		newLevelTween.Play(true);
		
		yield return new WaitForSeconds(.5f);
		
		isPlaying = true;

		this.StartCoroutine("CountdownTimerCoroutine");
		
		startNewLevelSignalSender.SendSignals(this);
	}
	
	IEnumerator CountdownTimerCoroutine()
	{
		if(countdownTimerIsRunning == false) {
		
			countdownTimerIsRunning = true;
			
			currentTimeRemaining = baseTimeLimit;
			
	
			while(currentTimeRemaining > 0) {
				
				currentTimeRemaining -= Time.deltaTime;
					
				yield return null;
			}
			
			if(currentTimeRemaining < 0) currentTimeRemaining = 0;
			
			countdownTimerIsRunning = false;
			
			// kill the player
			playerGO.GetComponent<Health>().OnDamage(1);
			
		
		}
	}

}
