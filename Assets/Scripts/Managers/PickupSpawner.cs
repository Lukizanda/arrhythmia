using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PickupSpawner : PausableMonobehaviour {
	
	public GameObject beatPickupPrefab;

	// Spawn chance properties
	public float spawnChancePerSecond = .10f;
	
	public List<GameObject> pickupsInScene = new List<GameObject>();
	
	public int minPickupsInScene = 2;
	public int maxPickupsOnScene = 5;
	
	private static PickupSpawner shared;
	public static PickupSpawner Shared {
		get { 
			if(shared == null)
				Debug.Log("Pickup spawner is not in the scene but something is trying to use it.");
			
			return shared; 
		}
	}
	
	void Awake()
	{
		shared = this;
	}
	
	void Start()
	{
		this.StartCoroutine("PickupSpawnerCoroutine");		
	}
	
	
	IEnumerator PickupSpawnerCoroutine()
	{
		while(true) {
						
			if(!paused && LevelController.Shared.isPlaying) {
				
				float randValue = Random.value;
				
				if(pickupsInScene.Count < minPickupsInScene) {
					SpawnPickupAndRandomPosition();
				}
				
				if(randValue <= spawnChancePerSecond && (pickupsInScene.Count < maxPickupsOnScene)) {
					
					SpawnPickupAndRandomPosition();
											
				}
			
			}
			
			yield return new WaitForSeconds(1.0f);
		}	
	}
	
	public void SpawnPickupAndRandomPosition()
	{
		Vector3 spawnPoint = GetValidPickupLocation();
		
		GameObject pickupGO = null;
		
		//pickupGO = Instantiate(unlockedPickups[Random.Range(0, unlockedPickups.Length)], spawnPoint, Quaternion.identity) as GameObject;
		pickupGO = Instantiate(beatPickupPrefab, spawnPoint, Quaternion.identity) as GameObject;
		
		pickupsInScene.Add(pickupGO);

		Intangible.Begin(pickupGO, 1.0f);
		
		FadeSprite.Begin(pickupGO, 1.0f);
			
	}
	
	Vector3 GetValidPickupLocation()
	{	
		Vector3 spawnPos = Vector3.zero;
		
		bool positionInvalid = true;
		
		while(positionInvalid) {
			
			Vector3 randomDirection = Random.onUnitSphere;
			
			randomDirection = new Vector3(randomDirection.x, randomDirection.y, 0.0f);
			
			RaycastHit hitInfo;			
				
			if(Physics.Raycast(Vector3.zero, randomDirection, out hitInfo, Mathf.Infinity, Layers.WallMask)) {
								
				Vector3 terminationPoint = hitInfo.point;
				
				Vector3[] linePoints = new Vector3[2];
				
				linePoints[0] = Vector3.zero; // at the center
				
				linePoints[1] = terminationPoint;
				
				spawnPos = iTween.PointOnPath(linePoints, Random.Range(0.1f, 0.9f));  // Dont spawn too near center and to near the walls
				
				if(!Physics.CheckSphere(spawnPos, 1f, Layers.EnemyMask)) {
					
					positionInvalid = false;
										
				}				
				
			}
			
		}
		
		return spawnPos;
	}
	
	/// <summary>
	/// Removes a pickup.
	/// </summary>
	/// <param name='pickupGO'>
	/// Pickup Gameobject
	/// </param>
	public void RemovePickup(GameObject pickupGO)
	{
		pickupsInScene.Remove(pickupGO);
		
		if(pickupGO)
			Destroy(pickupGO);
	}	
	
}
