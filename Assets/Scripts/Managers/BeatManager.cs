using UnityEngine;
using System.Collections;

public class BeatManager : MonoBehaviour {

	public float[] beatIntervals;
	public float[] pitchLevels;
	public float[] levelIntervals;
	
	public GameObject[] enemyTurretGOs;
	
	public SignalSender beatSignalSender;
	
	public UISlider beatSlider;
	
	bool beatCoroutineRunning = false;
	
	public float CurrentBeat {
		get { return currentBeat; }
		set { currentBeat = value; }
	}	
	private float currentBeat = .5f;
	
	private int currentLevel = 0;
	
	public float rateOfDecrease = 1.0f;
	
	private static BeatManager shared;
	public static BeatManager Shared {
		
		get {
			if(shared == null)
				Debug.Log("Something is trying to use beat manager, but it's not on the scene");
			return shared;
		}
		
	}
	
	void Awake()
	{
		shared = this;
	}
	
	void Start()
	{
		beatSlider.sliderValue = currentBeat;
		
		//ReceiverItem gameStartListener = new ReceiverItem(this.gameObject, "OnGameStart", 0.0f);
		//LevelController.Shared.gameStartSignalSender.receivers.Add(gameStartListener);
		
		ReceiverItem gameRestartListener = new ReceiverItem(this.gameObject, "OnNewLevel", 0.0f);
		LevelController.Shared.startNewLevelSignalSender.receivers.Add(gameRestartListener);
		
		ReceiverItem gameOverListener = new ReceiverItem(this.gameObject, "OnGameOver", 0.0f);
		LevelController.Shared.gameOverSignalSender.receivers.Add(gameOverListener);
			
	}
	
	void OnNewLevel() 
	{
		beatSlider.sliderValue = currentBeat;
		
		this.StartCoroutine("BeatCoroutine");
	}
	
	void OnGameOver()
	{
		Reset();
		
		this.StopAllCoroutines();
	}
	
	void OnGameRestart()
	{
		Reset();
		
		beatSlider.sliderValue = currentBeat;
		
		this.StartCoroutine("BeatCoroutine");
	}
	
	void Reset()
	{
		beatCoroutineRunning = false;
				
		currentBeat = .5f;
		
		currentLevel = 0;		
	}
		
	IEnumerator BeatCoroutine()
	{
		if(beatCoroutineRunning == false ) {
			
			beatCoroutineRunning = true;
		
			float timeRemaining = beatIntervals[currentLevel];
			
			audio.pitch = pitchLevels[currentLevel];
			
			audio.pitch = pitchLevels[currentLevel];
					
			audio.Play();
			
			beatSignalSender.SendSignals(this);
			
			while(enabled) {
			
				// reduce beat counter here
				beatSlider.sliderValue = Mathf.Clamp01(currentBeat -= Time.deltaTime * rateOfDecrease);
				
				currentLevel = GetBeatLevel();
				
				// send beat signals here
				if(timeRemaining > 0) {
					
					timeRemaining -= Time.deltaTime;
				
				} else {
					
					beatSignalSender.SendSignals(this);
					
					if(LevelController.Shared.isPlaying) {
					
						foreach(GameObject go in enemyTurretGOs) {
							
							go.GetComponent<BulletLauncher>().LaunchProjectile();
						
						}
						
					}
					
					audio.pitch = pitchLevels[currentLevel];
					
					audio.Play();
					
					timeRemaining = beatIntervals[currentLevel];
				
				}
				
				yield return null;
			}
		}
		
		yield return null;
	}
	
	int GetBeatLevel()
	{
		// check level 1 range
		if(currentBeat >= levelIntervals[0] && currentBeat <= (1f - levelIntervals[0])) {
			return 0;
		}
		
		// check level 2 range
		if((currentBeat >= levelIntervals[1] && currentBeat < levelIntervals[0]) ||
			(currentBeat <= (1f - levelIntervals[1]) && currentBeat > (1f - levelIntervals[0]))) {
			return 1;
		}
		
		// check level 3 range
		if((currentBeat >= levelIntervals[2] && currentBeat < levelIntervals[1]) ||
			(currentBeat <= (1f - levelIntervals[2]) && currentBeat > (1f - levelIntervals[1]))) {
			return 2;
		}
		
		// check level 4 range
		return 3;
	}
	
	
}
