using UnityEngine;
using System.Collections;

public class ScoreController : PausableMonobehaviour {

		// Accesible score controller properties
	public SignalSender scoreUpdateSignal;
	public SignalSender multiplierUpdateSignal;
	public float multiplierStreakTime = 3.0f;
	
	// public latest score added for other class who are interested to know
	public long recentlyAddedScore = 0;
	
	// Score controller variables
	private float streakTimeRemaining = 0;
	private bool multiplierStreakIsRunning = false;
	
	private int multiplier = 1;
	public int Multiplier {
		get { return multiplier; }
	}
	
	private long score = 0;
	public long Score {
		get { return score; }
	}
	
	private static ScoreController shared;
	public static ScoreController Shared {
		get { 
			if(shared == null) {
				Debug.Log("Something is trying to use the ScoreManager, but it hasn't been placed in the scene");
			}
			return shared;
		}
	}
	
	void Awake()
	{		
		shared = this;
	}
	
	void Start()
	{
		ReceiverItem levelStartedListener = new ReceiverItem(this.gameObject, "OnLevelStart", 0.0f);
		LevelController.Shared.startNewLevelSignalSender.receivers.Add(levelStartedListener);
		
		ReceiverItem gameOverListener = new ReceiverItem(this.gameObject, "OnGameOver", 0.0f);
		LevelController.Shared.gameOverSignalSender.receivers.Add(gameOverListener);
	}
	
	void OnLevelStart() 
	{
		this.score = 0;
				
		scoreUpdateSignal.SendSignals(this);
	}
	
	void OnGameOver()
	{
		Reset();
	}
	
	public void Reset()
	{
		//streakTimeRemaining = 0;
		//multiplier = 1;
		score = 0;
	}
	
	public void RecordPoints(int points, Vector3 position)
	{
		int totalScoreAdded = 0;
		
		if(multiplierStreakIsRunning) {
			
			multiplier ++;
			
			totalScoreAdded = points * multiplier;
		
		} else {
			
			totalScoreAdded = points;
			
		}
	
		FeedbackInformation feedbackInfo = new FeedbackInformation("+ "+totalScoreAdded, new Color(0.52f, 1.0f, 0.31f, .5f), 24, 20, 1.0f);
						
		VisualFeedbackHandler.SpawnFeedback(feedbackInfo, position);			
		
		this.StartCoroutine("StreakMultiplierCoroutine");

		score += totalScoreAdded;
		
		scoreUpdateSignal.SendSignals(this);
		
		if(score > LevelController.Shared.CurrentScoreTarget) {
			
			LevelController.Shared.WinLevel();
			
		}
	}
	
	
	IEnumerator SubscribeToGameSignals()
	{
		// Wait for managers to setup before listening to them
		yield return new WaitForSeconds(1.0f);
		
		ReceiverItem playerDeathListener = new ReceiverItem(this.gameObject, "OnPlayerDeath", 0.0f);
		Player.Shared.playerDeathSignal.receivers.Add(playerDeathListener);	
		
		if(LevelController.Shared != null) {
			ReceiverItem gameOverListener = new ReceiverItem(this.gameObject, "OnGameRestart", 0.0f);
			LevelController.Shared.gameRestartedSignalSender.receivers.Add(gameOverListener);
		}
	}
	
	IEnumerator StreakMultiplierCoroutine()
	{		
		if(multiplierStreakIsRunning) {
			
			this.multiplierUpdateSignal.SendSignals(this);
						
			streakTimeRemaining = multiplierStreakTime;
			
		} else {
			
			// refresh streak time
			streakTimeRemaining = multiplierStreakTime;
			
			multiplierStreakIsRunning = true;
												
			while(multiplierStreakIsRunning) {
								
				if(!paused)
					streakTimeRemaining -= Time.deltaTime;
				
				if(streakTimeRemaining < 0) {
										
					// reset multiplier properties
					multiplierStreakIsRunning = false;
					
					multiplier = 1;
															
					// send signal to people listening, like score UI
					multiplierUpdateSignal.SendSignals(this);
				}
				
				yield return null;
				
			}
			
		}
		
		yield return null;
		
	}
	
}
