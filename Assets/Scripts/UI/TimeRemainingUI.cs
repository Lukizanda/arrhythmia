using UnityEngine;
using System.Collections;

public class TimeRemainingUI : PausableMonobehaviour {
	
	public UILabel countdownLabel;
	string countdownString = "{0}:{1}";
	public BeatTriggeredScale beatTriggeredScale;
	public GameObject dangerSignGO;
	
	public float dangerTimeTrigger = 15f;

	// Update is called once per frame
	void Update () 
	{
		if(!paused) {
			
			countdownLabel.text = FloatToTimeMSString(LevelController.Shared.CurrentTimeRemaining);
			
			
			if(LevelController.Shared.CurrentTimeRemaining < 15f && LevelController.Shared.isPlaying) {
				if(dangerSignGO.active == false) {
					dangerSignGO.SetActiveRecursively(true);
					beatTriggeredScale.enabled = true;
				}
				
			} else {
				
				if(dangerSignGO.active == true) {
					dangerSignGO.SetActiveRecursively(false);
					beatTriggeredScale.enabled = false;
				}
				
			}
			
		}
		
	}
	
	string FloatToTimeMSString(float time) 
	{	
		int mins = Mathf.FloorToInt(Mathf.Clamp(time/60f, 0f, 60f));
		string minString = mins.ToString();
		
		float sec = time/60;
		
		sec = Mathf.RoundToInt((sec - Mathf.Floor(time/60)) * 60);
						
		string secString = sec.ToString();
		
		if(sec < 10) secString = "0"+secString;
		
		string finalizedTimeString = "";
		
		finalizedTimeString = string.Format("{0}:{1}", minString, secString);
			
		return finalizedTimeString;
	}	
}
