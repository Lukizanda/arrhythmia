using UnityEngine;
using System.Collections;

public class ScoreUI : MonoBehaviour {
	
	// Score
	public UILabel scoreLabel;
	
	// Level Goals
	public UILabel levelGoalLabel;
	
	// Multiplier properties
	public GameObject multiplierGO;
	public UILabel multiplierLabel;
	public Color multiplierLabelColor;
	public TweenScale multiplierScaleTweener;
	
	private static ScoreUI shared;
	public static ScoreUI Shared {
		get {
			if(shared == null)
				Debug.Log("Score UI is not in the scene, but something is trying to use it");
			
			return shared;
		}
	}
	
	void Awake()
	{
		shared = this;	
	}
	
	void Start()
	{
		ReceiverItem scoreUpdateSignal = new ReceiverItem(this.gameObject, "OnScoreUpdate", 0.0f);
		ScoreController.Shared.scoreUpdateSignal.receivers.Add (scoreUpdateSignal);
				
		ReceiverItem multiplierChangeListener = new ReceiverItem(this.gameObject, "OnMultiplierChangeSignal", 0.0f);
		ScoreController.Shared.multiplierUpdateSignal.receivers.Add(multiplierChangeListener);
		
		ReceiverItem levelStartListener = new ReceiverItem(this.gameObject, "OnLevelStart", 0.0f);
		LevelController.Shared.startNewLevelSignalSender.receivers.Add(levelStartListener);
		
		ReceiverItem levelWinListener = new ReceiverItem(this.gameObject, "OnLevelWin", 0.0f);
		LevelController.Shared.levelWinSignalSender.receivers.Add(levelWinListener);
		
		scoreLabel.text = "0";
	}
	
	void OnLevelStart()
	{
		levelGoalLabel.text = "GOAL - [FFFFFF]" + LevelController.Shared.CurrentScoreTarget.ToString();
		scoreLabel.text = ScoreController.Shared.Score.ToString();
	}
	
	void OnLevelWin()
	{
		levelGoalLabel.text = "GOAL - [FFFFFF]" + LevelController.Shared.CurrentScoreTarget.ToString();
		scoreLabel.text = ScoreController.Shared.Score.ToString();
	}
	
	public void Reset()
	{
		levelGoalLabel.text = "GOAL - [FFFFFF]" + LevelController.Shared.baseScoreTarget;
		scoreLabel.text = "0";
	}
	
	void OnScoreUpdate()
	{
		scoreLabel.text = ScoreController.Shared.Score.ToString();
	}
	
	void OnMultiplierChangeSignal ()
	{
		if(ScoreController.Shared.Multiplier > 1) {
			
			if(!multiplierGO.active) multiplierGO.SetActiveRecursively(true);
			
			// Make sure multiplier labels are back to their original size and color
			foreach(Transform t in multiplierGO.transform) {
				TweenColor.Begin(t.gameObject, .1f, multiplierLabelColor);
			}
		
			multiplierGO.transform.localScale = Vector3.one;
						
			//multiplierLabel.text = "X " + ScoreController.Shared.Multiplier.ToString();
			multiplierLabel.text = "MULTIPLIER x" + ScoreController.Shared.Multiplier.ToString();

		
			// animate multiplier text
			//multiplierGO.transform.localScale = new Vector3(1.2f, 1.2f, 1.0f);
			multiplierGO.transform.localScale = Vector3.one;

			multiplierScaleTweener = TweenScale.Begin(multiplierGO, .4f, Vector3.one);
			multiplierScaleTweener.callWhenFinished = "FadeAndScaleOffMultiplierText";
			multiplierScaleTweener.eventReceiver = this.gameObject;
	
			multiplierScaleTweener.enabled = true;


		} else {
			
			multiplierGO.SetActiveRecursively(false);
			
		}
	}

	void FadeAndScaleOffMultiplierText()
	{				
		float fadeTime = ScoreController.Shared.multiplierStreakTime - .4f;
				
		foreach(Transform t in multiplierGO.transform) {
			TweenColor.Begin(t.gameObject, fadeTime, Color.clear);
		}
		
		//multiplierScaleTweener = TweenScale.Begin(multiplierGO, fadeTime, new Vector3(.8f, .8f, 1.0f));
		
		multiplierScaleTweener.callWhenFinished = "";
		//multiplierScaleTweener.from = Vector3.one;
						
		multiplierScaleTweener.enabled = true;
		
	}	
	
}
