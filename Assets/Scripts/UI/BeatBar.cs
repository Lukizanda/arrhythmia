using UnityEngine;
using System.Collections;

public class BeatBar : MonoBehaviour {
	
	public GameObject glowSpriteGO;
	public TweenColor glowTween;
	
	// Use this for initialization
	void Start () 
	{
		ReceiverItem beatListener = new ReceiverItem(this.gameObject, "OnBeat", 0.0f);	
		BeatManager.Shared.beatSignalSender.receivers.Add(beatListener);
	
	}
	
	void OnBeat()
	{
		glowTween.Reset();
		glowTween.Play(true);
	}
	
}
