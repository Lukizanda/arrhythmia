using UnityEngine;
using System.Collections;

public class BeatPickup : Pickup {

	public float beatIncrease = 0.02f;
	public int points = 1;
	
	public override void Activate ()
	{
		base.Activate ();
		
		BeatManager.Shared.CurrentBeat += beatIncrease;
			
		PickupSpawner.Shared.RemovePickup(this.gameObject);
	}
	
	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.layer == Layers.PlayerLayer) {
			
			ScoreController.Shared.RecordPoints(points, transform.position);
		
			this.Activate();
		
		}
	}
	
	void DestroySelf()
	{
		PickupSpawner.Shared.RemovePickup(this.gameObject);
	}
}
