using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour {
	
	public GameObject activationSFXprefab;
	public GameObject activationEffectPrefab;

	public virtual void Activate()
	{
		if(activationSFXprefab != null) 
			PoolManager.Pools["Audio"].Spawn(activationSFXprefab.transform);
			
		if(activationEffectPrefab != null)
			PoolManager.Pools["Explosion"].Spawn(activationEffectPrefab.transform);
	}
		
}
