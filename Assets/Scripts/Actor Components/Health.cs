using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour
{
    public float maxHealth;
    public float currentHealth;
	public float regenerationAmount = 0.0f;
	public float regenerationTickSpeed = 1.0f;
	public bool invincible;
	public bool damageFeedback = false;
	GameObject damageFeedbackPrefab;
	
	private bool isDead = true; // set default to death since we don't know whether the object is active or not.
	public bool IsDead {
		get { return isDead; }
		set { isDead = value; } 
	}

	public bool canBeKilled = true;

	public SignalSender damageSignals;
	public SignalSender deathSignals;
	public SignalSender reviveSignals;
	
	// Note for C# and Boo users: use Awake instead of the constructor for initialization, 
	// as the serialized state of the component is undefined at construction time. 
	// Awake is called once, just like the constructor.
	void Awake() 
	{
		if(this.gameObject.active) {
			currentHealth = maxHealth;
			damageFeedbackPrefab = Resources.Load("Visual Feedback/Damage Feedback") as GameObject;
			this.isDead = false;
		}	
	}
	
	void OnEnable()
	{
		this.Reset();
		StartCoroutine("Regenerate");
	}
	
	void OnDisable()
	{
		StopCoroutine("Regenerate");
	}
	
	public void Reset() 
	{
		currentHealth = maxHealth;
		isDead = false;
	}
	
    public void OnDamage(float damageAmount)
    {	
		if(invincible)
			return;
		if(isDead)
			return;
		if(damageAmount<=0)
			return;
		
		// Visual feedback if enabled;
		if(damageFeedback){
			Transform t = PoolManager.Pools["Effect"].Spawn(damageFeedbackPrefab.transform);
			t.localPosition = Vector3.zero;
			t.localRotation = Quaternion.identity;
			t.localScale = damageFeedbackPrefab.transform.localScale;
			t.GetComponent<DamageFeedback>().BeginDamageFeedback(damageAmount.ToString(), this.transform);
		}
				
        currentHealth = Mathf.Max(currentHealth - damageAmount, 0);
		
		damageSignals.SendSignals(this);
		
		if(currentHealth <=0 && canBeKilled)
			isDead = true;
		
		// Send death signals if the object is dead
		if(isDead) {
			deathSignals.SendSignals(this);
		}
		
    }

    public void OnHeal(float healAmount)
    {
        currentHealth = Mathf.Min(currentHealth + healAmount, maxHealth);
    }
	
	IEnumerator Regenerate() {
				
		if(regenerationAmount > 0) {
			while(enabled) {
				if(currentHealth < maxHealth) {
					currentHealth = Mathf.Min(currentHealth + regenerationAmount, maxHealth);
				}
				yield return new WaitForSeconds(regenerationTickSpeed);
			}	
		}
		
	}
	
	public void Kill() {
		isDead = true;
		currentHealth = 0;
		deathSignals.SendSignals(this);
	}
	
	public void Revive() 
	{
		isDead = false;
		currentHealth = maxHealth;
		reviveSignals.SendSignals(this);
	}
	
	public void Revive(float healthAmount) 
	{
		isDead = false;
		currentHealth = healthAmount;
		reviveSignals.SendSignals(this);
	}
}
