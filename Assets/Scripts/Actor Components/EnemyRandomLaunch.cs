using UnityEngine;
using System.Collections;

public class EnemyRandomLaunch : MonoBehaviour {
	
	public float pushForce = 50;
	public float torquePush = 10;
	
	void Start()
	{
		if(rigidbody.isKinematic) rigidbody.isKinematic = false;
		Vector3 randomDirection = Random.onUnitSphere;
		randomDirection = new Vector3(randomDirection.x, randomDirection.y, 0.0f);
		
		//Vector3 randomForce = new Vector3(Random.Range(-forcePush, forcePush), Random.Range(-forcePush, forcePush), 0.0f);
		Vector3 randomTorque = new Vector3(0.0f, 0.0f, Random.Range(-torquePush, torquePush));
		this.rigidbody.AddForce(randomDirection * pushForce, ForceMode.Impulse);
		this.rigidbody.AddRelativeTorque(randomTorque, ForceMode.Impulse);		
	}
	
}
