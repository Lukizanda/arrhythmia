using UnityEngine;
using System.Collections;

public class Intangible : MonoBehaviour {

	public float timeRemaining = 0;
	
	int originalLayer;
		
	/// <summary>
	/// Raises the enable event. Store original values and switch to phased layer so that object doesnt collide with anything
	/// </summary>
	void OnEnable()
	{
		originalLayer = gameObject.layer;
		gameObject.layer = Layers.PhasedLayer;
	}
	
	
	/// <summary>
	/// Raises the disable event. Make sure object switches back to its original layer.
	/// </summary>
	void OnDisable()
	{
		if(this.gameObject.layer == Layers.PhasedLayer)
			this.gameObject.layer = originalLayer;
	}
	
	void Update()
	{
		if(timeRemaining > 0) {
			
			timeRemaining -= Time.deltaTime;

		} else {
			
			if(this.gameObject.layer == Layers.PhasedLayer)
				this.gameObject.layer = originalLayer;
			
			this.enabled = false;
			
		}
	}
	
	
	/// <summary>
	/// Begin intangile on the specified go, with duration and fadeEnabled.
	/// </summary>
	/// <param name='go'>
	/// The game object affected
	/// </param>
	/// <param name='duration'>
	/// Duration.
	/// </param>
	/// <param name='fadeEnabled'>
	/// Fade enabled.  Whether object will visually fade in.
	/// </param>
	static public void Begin(GameObject go, float duration) 
	{							
		/// Recurse through object and make intangible what can be intangible	
			
		Intangible intangibleEffect = go.GetComponent<Intangible>();
		
		if(intangibleEffect == null) {
			intangibleEffect = go.AddComponent<Intangible>();
			
		} 						

		intangibleEffect.timeRemaining = duration;
								
		if(intangibleEffect.enabled == false) intangibleEffect.enabled = true;
			
		
		
		foreach(Transform child in go.transform) {
			
			Intangible.Begin(child.gameObject, duration);
			
		}

		
	
	}
	
}
