using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	
	[HideInInspector]
	public Health health;
	
	// Graphical properties
	public Transform postDestructionPrefab;
	public GameObject audioEffectsPrefab;	
	
	public SignalSender playerDeathSignal;
	
	private static Player shared;
	public static Player Shared {
		get {
			if(shared == null)
				Debug.Log("Player has not been instantiated in the scene but something is trying to access it");
			
			return shared;
		}
	}
	
	
	void Awake() 
	{
		shared = this;
		health = this.GetComponent<Health>();
	}
	
	void Death() {
		
		if(postDestructionPrefab)
			PoolManager.Pools["Explosion"].Spawn(postDestructionPrefab, this.transform.position, Quaternion.identity);

		//this.renderer.enabled = false;
				
		playerDeathSignal.SendSignals(this);
				
		// If audio effects available
		if(audioEffectsPrefab)
			PoolManager.Pools["Audio"].Spawn (audioEffectsPrefab.transform);
			//Instantiate(audioEffectsPrefab);
		
		health.Reset();
		
		this.transform.position = Vector3.zero;
	}
	
	
}
