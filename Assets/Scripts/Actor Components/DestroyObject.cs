using UnityEngine;
using System.Collections;

public class DestroyObject : MonoBehaviour {
	
	public string spawnPoolName = "";
	public Transform objectToDestroy;
	public Transform postDestructionPrefab;
	public GameObject[] audioEffectPrefabs;
	
	void Start()
	{
		if(objectToDestroy == null)
			objectToDestroy = this.transform;
		
	}
	
	void OnDestroySignal() 
	{
		if(spawnPoolName.Length > 0) {
			PoolManager.Pools[spawnPoolName].Despawn(objectToDestroy);
		} else {
			Destroy(objectToDestroy.gameObject);
		}
		
		/// Instantiate the post destruction object if it is available
		if(postDestructionPrefab) 
			PoolManager.Pools["Explosion"].Spawn(postDestructionPrefab, new Vector3(transform.position.x, transform.position.y, -7), Quaternion.identity);
		
		
		/// Instantiate audio fx if available
		if(audioEffectPrefabs != null) {
			if(audioEffectPrefabs.Length > 0) {
				PoolManager.Pools["Audio"].Spawn (audioEffectPrefabs[Random.Range(0, audioEffectPrefabs.Length)].transform);
			}
		}
	}
	
	void OnSelfDestructSignal()
	{
		
		if(spawnPoolName.Length > 0) {
			PoolManager.Pools[spawnPoolName].Despawn(objectToDestroy);
		} else {
			Destroy(objectToDestroy.gameObject);
		}
		
		// Instantiate the post destruction object if it is available
		if(postDestructionPrefab) {
			PoolManager.Pools["Explosion"].Spawn(postDestructionPrefab, new Vector3(transform.position.x, transform.position.y, -7), Quaternion.identity);
		}
	}
	
}
