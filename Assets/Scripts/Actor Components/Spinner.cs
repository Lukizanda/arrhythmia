using UnityEngine;
using System.Collections;

public class Spinner : PausableMonobehaviour {
	
	public float maxRotationVelocity = 10f;
	
	public float currentRotationVelocity = 0.0f;
	
	public float acceleration = 0.0f;
	
	public float targetVelocity = 100.0f;
	
	public bool breakApplied;
	
	float breakForce = 30f;
	
	bool wasPaused = false;
	float savedVelocity = 0;
		
	// Update is called once per frame
	void Update () {
		
		if(!paused) {
			
			if(wasPaused) {
				currentRotationVelocity = savedVelocity;
				savedVelocity = 0;
				wasPaused = false;
			}
			
			if(breakApplied) {
				
				float previousVelocity = currentRotationVelocity;
				
				currentRotationVelocity = currentRotationVelocity - (Time.deltaTime * breakForce * Mathf.Sign(acceleration));
				
				if(Mathf.Sign(previousVelocity) != Mathf.Sign(currentRotationVelocity) || previousVelocity == 0) currentRotationVelocity = 0;
				
			} else {
				
				currentRotationVelocity += Time.deltaTime * acceleration;
			
			}
			
			if(Mathf.Abs(currentRotationVelocity) > maxRotationVelocity) {
				currentRotationVelocity = maxRotationVelocity * Mathf.Sign(currentRotationVelocity);
			}
						
			transform.Rotate(Vector3.forward * Time.deltaTime * currentRotationVelocity, Space.World);
		
		} else {
			
			if(!wasPaused) {
				savedVelocity = currentRotationVelocity;
				wasPaused = true;
			}
		}
	}
	
	void OnEnable()
	{
		this.paused = false;
	}
	
	public void FlipAccelerationDirection()
	{
		this.acceleration = -(this.acceleration);
	}
	
	/// <summary>
	/// Instantly reduce current rotation velocity to 0.
	/// </summary> 
	public void EmergencyBreak()
	{
		currentRotationVelocity = 0;
	}
	
	public void Stop()
	{
		this.enabled = false;
	}
	
	public void ApplyBreaks(float breakForce) 
	{
		breakApplied = !breakApplied;
		this.breakForce = breakForce;
	}
	
}
