using UnityEngine;
using System.Collections;

public class PausableMonobehaviour : MonoBehaviour {
	
	protected bool paused = false;
	
	void OnPauseGame() 
	{
		paused = true;
	}
	
	void OnResumeGame()
	{
		paused = false;
	}
	
}
