using UnityEngine;

[System.Serializable]
public class Line : System.Object {
	
	private Vector3 startPoint;
	private Vector3 endPoint;
	private float yIntercept;

	private float gradient;
	public float Gradient {
		get { return gradient; }
	}
	
	private Vector3 midPoint;
	public Vector3 MidPoint {
		get { return midPoint; }
	}
	
	public Line(Vector3 startPoint, Vector3 endPoint) 
	{	
		this.startPoint = startPoint;
		this.endPoint = endPoint;
		this.gradient = (endPoint.y - startPoint.y) / ( endPoint.x - startPoint.x);
		this.yIntercept = startPoint.y - (gradient * startPoint.x);
		this.midPoint = (endPoint + startPoint)/2;
	}
	
	public Vector3 GetPerpendicularVector() {
		
		float inverseGradient = -(1/this.gradient);
		float c = midPoint.y - (inverseGradient * midPoint.x);
	
		float x = 3;
		float y = (inverseGradient * x) + c;
		
		Vector3 tempPoint = new Vector3(x, y, 0); 
		Vector3 lineDirection = (tempPoint - midPoint).normalized;	
		
		return lineDirection;
	}
	
	public Vector3 GetPointWhereXEquals(float x) 
	{	
		float y = gradient * x + yIntercept;
		return new Vector3(x, y, 0);
	}
	
	/// <summary>
	/// Gets the point on line.
	/// </summary>
	/// <returns>
	/// The point on line.
	/// </returns>
	/// <param name='partOfLine'>
	/// The percentage value of which part of the line you want to be at.
	/// </param>
	public Vector3 GetPointOnLine(float partOfLine)
	{
		partOfLine = Mathf.Clamp01 (partOfLine);
		Vector3 differenceBetweenPoints = endPoint - startPoint;
		
		return startPoint + differenceBetweenPoints * partOfLine;
	}
	
	public Vector3 PointOnPerpendicularLineThrough(Vector3 pointOnLine, float offset) {
		
		float inverseGradient = -(1/this.gradient);
		//Debug.Log(gradient + " vs " +inverseGradient);
		// get a temp point so we can get a directional vector, put it as 10;
		float c = pointOnLine.y - (inverseGradient * pointOnLine.x);
		//Debug.Log("c = " +c);
		float x = 3;
		float y = (inverseGradient * x) + c;
		Vector3 tempPoint = new Vector3(x, y, 0); 
		Vector3 lineDirection = (tempPoint - pointOnLine).normalized;
		
		return pointOnLine + (lineDirection * offset);
	}
	
}
