using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SignalSender {

	public bool onlyOnce;
	public List<ReceiverItem> receivers;
	private bool hasFired = false;
	
	public void SendSignals (MonoBehaviour sender) 
	{
		if(hasFired == false || onlyOnce == false) {
			for(int i = 0; i < receivers.Count ;  i++) {
				sender.StartCoroutine(receivers[i].SendWithDelay(sender));
			}
			hasFired = true;
		}
	}
	
	public void SendSignals (MonoBehaviour sender, object value) 
	{
		if(hasFired == false || onlyOnce == false) {
			for(int i = 0; i < receivers.Count ;  i++) {
				sender.StartCoroutine(receivers[i].SendWithDelayAndParameter(sender, value));
			}
			hasFired = true;
		}
	}
	
}
