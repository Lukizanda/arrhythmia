using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UILabel))]
public class DamageFeedback : MonoBehaviour {
		
	UILabel label;
	public float duration = 1.5f;
	public float yDelta = 50.0f;
	TweenScale scaleTweener;
	TweenColor colorTweener;
	
	void Awake()
	{
		label = this.gameObject.GetComponent<UILabel>();
		scaleTweener = this.gameObject.GetComponent<TweenScale>();
		colorTweener = this.gameObject.GetComponent<TweenColor>();
		scaleTweener.duration = duration / 2 ;
		colorTweener.from = new Color(label.color.r, label.color.g, label.color.b, 0);
		colorTweener.to = label.color;
	}
	
	public void BeginDamageFeedback(string damageString, Transform target)
	{
		label.text = damageString;
		scaleTweener.Play(true);
		colorTweener.Play(true);
		this.StartCoroutine(TweenPositionRelativeToTransform(target));
	}
	
	IEnumerator TweenPositionRelativeToTransform(Transform target)
	{
		float startTime = Time.time;
		float speed = yDelta/duration;
		float yOffset = target.collider.bounds.size.y / 2 ;
		float xOffset = target.collider.bounds.size.x / 2;
				
		while(Time.time - startTime < duration && target != null) {
			
			//transform.localPosition = target.position + (Vector3.right * xOffset) + (Vector3.up * yOffset);
			transform.localPosition = target.position + (Vector3.up * yOffset) + (Vector3.right * xOffset);
			yOffset += Time.deltaTime * speed;
			
			yield return null;
		}
		
		this.Reset();
	}
	
	void Reset()
	{
		scaleTweener.Reset();
		colorTweener.Reset();
		PoolManager.Pools["Effect"].Despawn(this.transform);		
	}

}
