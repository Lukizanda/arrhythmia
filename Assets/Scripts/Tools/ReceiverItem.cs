using UnityEngine;
using System.Collections;

[System.Serializable]
public class ReceiverItem {
	
	public GameObject receiver;
	public string action = "OnSignal";
	public float delay;
	
	public ReceiverItem() {
		this.receiver = null;
		this.action = "";
		this.delay = 0;
	}
	
	public ReceiverItem(GameObject receiver, string action, float delay) {
		this.receiver = receiver;
		this.action = action;
		this.delay = delay;
	}
	
	public IEnumerator SendWithDelay (MonoBehaviour sender) 
	{
		yield return new WaitForSeconds(delay);
	
		if(receiver)
			receiver.SendMessage(action, SendMessageOptions.DontRequireReceiver);
		else
			Debug.LogWarning("No receiver of signal \""+action+"\" on object "+ sender.name+" ("+sender.GetType().Name+")", sender);
		
	}
	
	public IEnumerator SendWithDelayAndParameter (MonoBehaviour sender, object value) 
	{
		yield return new WaitForSeconds(delay);
	
		if(receiver)
			receiver.SendMessage(action, value);
		else
			Debug.LogWarning("No receiver of signal \""+action+"\" on object "+ sender.name+" ("+sender.GetType().Name+")", sender);		
		
	}
	
}
