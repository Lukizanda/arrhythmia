using UnityEngine;
using System.Collections;

public static class PointTools {
	
	static float wallPaddingInUnits = 5f;
	
	public static bool PointInPolygon(Vector2[] vertices, Vector2 point)
	{
		int j = vertices.Length - 1;
		bool oddNodes = false;
		/*
		 for (i=0; i<polySides; i++) {
		    if ((polyY[i]< y && polyY[j]>=y
		    ||   polyY[j]< y && polyY[i]>=y)
		    &&  (polyX[i]<=x || polyX[j]<=x)) {
		      if (polyX[i]+(y-polyY[i])/(polyY[j]-polyY[i])*(polyX[j]-polyX[i])<x) {
		        oddNodes=!oddNodes; }}
		    j=i; }
		
		  return oddNodes; }
				
		*/
		for(int i = 0; i < vertices.Length; i ++ ) {
			if((vertices[i].y < point.y && vertices[j].y >= point.y
				|| vertices[j].y < point.y && vertices[i].y >= point.y)
				&& (vertices[i].x <= point.x || vertices[j].x <= point.x)) {
				
				if(vertices[i].x + (point.y - vertices[i].y)/(vertices[j].y - vertices[i].y) * (vertices[j].x - vertices[i].x) < point.x) {
					oddNodes = !oddNodes; }}
			
			j = i; 
		}
			
		return oddNodes;
		
	}
	
	public static bool PointIn3by2Walls(Vector3 point)
	{
		float sceneUnitWidth = Camera.mainCamera.orthographicSize * 3;
		float sceneUnitHeight = Camera.mainCamera.orthographicSize * 2;
		
		float widthFromCenter = sceneUnitWidth/2 - wallPaddingInUnits;
		float heightFromCenter = sceneUnitHeight/2 - wallPaddingInUnits;
		
		if((point.x < -widthFromCenter || point.x > widthFromCenter) ||
			(point.y < -heightFromCenter || point.y > heightFromCenter)) {
			
			return false;
		}
		
		return true;
	}
	
	
}
