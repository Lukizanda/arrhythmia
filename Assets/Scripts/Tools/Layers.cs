using UnityEngine;
using System.Collections;

public static class Layers {
	
	public static int IgnoreRaycastLayer = LayerMask.NameToLayer("Ignore Raycast");
	public static int TrailLayer = LayerMask.NameToLayer("Trail");
	public static int PlayerLayer = LayerMask.NameToLayer("Player");
	public static int PlayerIntangibleLayer = LayerMask.NameToLayer("PlayerIntangible");
	public static int EnemyLayer = LayerMask.NameToLayer("Enemy");
	public static int PhasedLayer = LayerMask.NameToLayer("Phased");
	public static int UILayer = LayerMask.NameToLayer("UI");
	public static int WallLayer = LayerMask.NameToLayer("Wall");
	public static int DestructableLayer = LayerMask.NameToLayer("Destructable");
	public static int PickupLayer = LayerMask.NameToLayer("Pickup");
	public static int PhasedTrailLayer = LayerMask.NameToLayer("PhasedTrail");
	
	public static LayerMask PlayerMask = 1 << Layers.PlayerLayer;
	public static LayerMask WallMask = 1 << Layers.WallLayer;
	public static LayerMask EnemyMask = 1 << Layers.EnemyLayer;
	public static LayerMask TrailMask = 1 << Layers.TrailLayer;
	public static LayerMask IgnoreTrailMask = ~(1 << Layers.TrailLayer);
	public static LayerMask EnemyAndTrailMask = EnemyMask | TrailMask;
	public static LayerMask IgnorePickupMask = ~(1 << Layers.PickupLayer);
	public static LayerMask IgnorePhasedMask = ~(1 << Layers.PhasedLayer);
	public static LayerMask IgnorePlayerMask = ~(1 << Layers.PlayerLayer);
	public static LayerMask IgnoreEnemyMask = ~(1 << Layers.EnemyLayer);
	public static LayerMask IgnoreEnemyAndTrailMask = IgnoreEnemyMask & IgnoreTrailMask & IgnorePhasedMask;
	
	public static LayerMask IgnoreTangibleObjectMask = Layers.IgnorePlayerMask & Layers.IgnoreEnemyMask;
	public static LayerMask ObstacleMask = WallMask & TrailMask;
	public static LayerMask PlayerAndTrailMask = TrailMask & PlayerMask;

}
