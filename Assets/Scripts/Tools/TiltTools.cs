using UnityEngine;
using System.Collections;

public static class TiltTools {

	private static float verticalCenter = 0.0f;
	private static float horizontalCenter = 0.0f;
	private static float zPitchCenter = 0.0f;
	private static float verticalTurningPoint = 0.0f;	
	//private static float horizontalTurningPoint = 0.0f;
	public static bool horizontalSteeringEnabled = false;
	public static bool verticalSteeringEnabled = false;
	private static bool m_LandscapeRightEnabled = false;
	public static bool invalidPosition = false;
	public static bool verticalZAccelerationEnabled = false;
	
	/// <summary>
	/// Calculates the tilt of the device and returns a vector that tells the movement in 2d space
	/// </summary>
	/// <param name='devicePositionVector'>
	/// The zeroed/centered position that the player is playing in
	/// </param>
	
	public static void SetDevicePosition(Vector3 devicePositionVector, bool landscapeRightEnabled) 
	{	
		horizontalCenter = devicePositionVector.y;		
		
		verticalCenter = devicePositionVector.x;
		
		zPitchCenter = devicePositionVector.z;
		
		verticalTurningPoint = -verticalCenter;
		
		m_LandscapeRightEnabled = landscapeRightEnabled;
		
		// turn on horizontal steering if device is too upright vertically
		if((zPitchCenter > -0.35f && zPitchCenter <= 0.5f) && Mathf.Abs(verticalCenter) > 0.80f ) {
			
			horizontalSteeringEnabled = true;
			
		} else {
			
			horizontalSteeringEnabled = false;
			
		}
		
		// turn on vertical steering if device is too upright horizontally
		// set invalid position flag if this is true
		if(Mathf.Abs(zPitchCenter) < 0.2f && Mathf.Abs(horizontalCenter) > 0.85f) {

			invalidPosition = true;
			
		} else {
			
			invalidPosition = false;
			
		}
		
		if(Mathf.Abs(verticalCenter) > 0.85f) {
			
			verticalZAccelerationEnabled = true;
			
		} else if(Mathf.Abs(zPitchCenter) > 0.85f) {
			
			verticalZAccelerationEnabled = false;
			
		} else {
			
			verticalZAccelerationEnabled = false;
			
		}
		
	}
	
	public static Vector2 CalculateTilt()  
	{	
		float xAcceleration = Input.acceleration.x;
		float yAcceleration = Input.acceleration.y;
		float zAcceleration = Input.acceleration.z;
		
		// Calculate vertical tilt
			
		float verticalTilt = CalculateVerticalTilt (xAcceleration, zAcceleration);
		float horizontalTilt = CalculateHorizontalTilt(yAcceleration, xAcceleration, zAcceleration);
				
		return new Vector2(verticalTilt, horizontalTilt);
	}
			

	private static float CalculateHorizontalTilt (float yAcceleration, float xAcceleration, float zAcceleration)
	{
		float horizontalTilt = 0;
		
		if(horizontalSteeringEnabled) {
			
			// we can't detect if device over tilts, so we return the best estimation of the tilt
							
			if(yAcceleration > horizontalCenter) {
					
				horizontalTilt = -(horizontalCenter - yAcceleration);
					
			} else {
					
				horizontalTilt = yAcceleration - horizontalCenter;
				
			}			
			
			if(m_LandscapeRightEnabled) horizontalTilt = - horizontalTilt;
			
			return horizontalTilt;
	
		}
		
		if(zPitchCenter < 0) {
			
			//if(zAcceleration < 0) {
				
				horizontalTilt = yAcceleration - horizontalCenter;
			//}
			/*
			// we also need to check if user is flipping device because he is trying to tilt vertically
			
			if(zAcceleration > 0 && yAcceleration >= horizontalTurningPoint) {
				
				horizontalTilt = (1 - horizontalCenter) + (1 - yAcceleration);
				
			} else if(zAcceleration > 0 && yAcceleration < horizontalTurningPoint) {
				
				horizontalTilt = (-horizontalCenter - 1f) - ( 1 + yAcceleration);
				
			}
			*/
		}
		
		if(zPitchCenter > 0) {
			
			//if(zAcceleration > 0) {
				
				horizontalTilt = horizontalCenter - yAcceleration;
				
			//}
			/*
			if(zAcceleration < 0 && yAcceleration <= horizontalTurningPoint) {
				
				horizontalTilt = (1f + yAcceleration) + (1 + horizontalCenter);
				
			} else if(zAcceleration < 0 && yAcceleration > horizontalTurningPoint) {
				
				horizontalTilt = horizontalCenter - 1f - ( 1- yAcceleration);
				
			}
			*/
		}
		
		// Flip direction if device is in landscape right
		if(m_LandscapeRightEnabled) horizontalTilt = - horizontalTilt;
		
		return horizontalTilt;
	}
			
	private static float CalculateVerticalTilt (float xAcceleration, float zAcceleration)
	{
		float verticalTilt = 0;
		
		// x acceleration wont be accurate, so we're using the z axis
		
		if(verticalZAccelerationEnabled) {
		
			// device set slightly face up
			if(zPitchCenter < 0.0f) {
				
				// z acceleration is still facing up and is less then the z center (this is going up)
				if(zAcceleration < 0.0f && zAcceleration < zPitchCenter) {
					
					verticalTilt = zPitchCenter - zAcceleration;
				
					return verticalTilt;
				} 
				
				// z acceleration is still facing up and is more then the z center (this is going down)
				if(zAcceleration < 0.0f && zAcceleration > zPitchCenter) {
					
					verticalTilt = zPitchCenter - zAcceleration;
										
					return verticalTilt;
				}
				
				// z acceleration is facing down. goin down
				if(zAcceleration >= 0.0f) {
					
					verticalTilt  =  zPitchCenter - zAcceleration;
					
					return verticalTilt;
				}
				
			}
		
			// if device set slightly face down
			if(zPitchCenter > 0.0f) {
				
				// z acceleration is facing down and is more than the z center (this is going down)
				if(zAcceleration > zPitchCenter) {
					
					verticalTilt = zPitchCenter - zAcceleration;
				
					return verticalTilt;
				} 
				
				// z acceleration is still facing down but is more than the pitch center (this is going up)
				if(zAcceleration > 0.0f && zAcceleration < zPitchCenter) {
					
					verticalTilt = zPitchCenter - zAcceleration;
										
					return verticalTilt;
				}
				
				// z acceleration is now facing up.
				if(zAcceleration <= 0.0f) {
					
					verticalTilt  = -zAcceleration + zPitchCenter;
					
					return verticalTilt;
				}
				
			}			
					
		}
		
		// Normal tilt calculation using x and y axis
		// Device was set facing up
		if(zPitchCenter < 0) {
			
			// device is facing up, so we just get the center minus the current x acceleration
			if(zAcceleration < 0) {
				
				verticalTilt = verticalCenter - xAcceleration;
				
			}
			
			//device is now facing down, and is more than the vertical turning point, which means it just passed the vertical position
			
			if(zAcceleration > 0 && xAcceleration > verticalTurningPoint) {
				
				verticalTilt = (verticalCenter - 1f) - (1f - xAcceleration);
				
			}
			
			// device is facing down, and is less than the vertical turning point, meaning it's tilting over the vertical turning point
			if(zAcceleration > 0 && xAcceleration < verticalTurningPoint) {
				
				verticalTilt = (verticalCenter + 1f) + (1 + xAcceleration);
				
			}
				
			// Flip directions if default orientation is set to landscape right
			if(!m_LandscapeRightEnabled) verticalTilt = - verticalTilt;
			
			return verticalTilt;			
		}
		
		// Device was set facing down
		if(zPitchCenter > 0) {
			
			// device facing down
			if(zAcceleration > 0) {
				
				verticalTilt = xAcceleration - verticalCenter;
				
			}
			
			//if(zAcceleration > 0 && xAcceleration 
			
			// facing up and negative from turning point (going down)
			if(zAcceleration < 0 && xAcceleration < verticalTurningPoint) {
				
				verticalTilt = (-verticalCenter - 1f) - (1f + xAcceleration);
				
			}
			
			if(zAcceleration < 0 && xAcceleration > verticalTurningPoint) {
				
				verticalTilt = (1f - verticalCenter) + (1f - xAcceleration);
				
			}
			// Flip directions if default orientation is set to landscape right
			if(!m_LandscapeRightEnabled) verticalTilt = - verticalTilt;
			
			return verticalTilt;			
		}
		
		// Flip directions if default orientation is set to landscape right
		if(!m_LandscapeRightEnabled) verticalTilt = - verticalTilt;
		

		return verticalTilt;
		
	}
}
