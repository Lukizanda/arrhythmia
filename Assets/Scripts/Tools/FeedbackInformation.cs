using UnityEngine;
using System.Collections;

[System.Serializable]
public class FeedbackInformation {
	
	[SerializeField]
	private string displayString;
	public string DisplayString {
		get { return displayString; }
	}
	
	[SerializeField]
	private Color labelColor = Color.white;
	public Color LabelColor {
		get { return labelColor; }
	}
	
	[SerializeField]
	private float yOffset = 0.0f;
	public float YOffset {
		get { return yOffset; }
		set { yOffset = value; }
	}
	
	[SerializeField]
	private float duration;
	public float Duration {
		get { return duration; }
		set { duration = value; }
	}
	
	[SerializeField]
	private int fontSize = 33; // Default font size unless overriden
	public int FontSize {
		get { return fontSize; }
		set { fontSize = value; }
	}
	
	public FeedbackInformation(string displayString, float duration) 
	{
		this.displayString = displayString;
		this.duration = duration;
	}
	
	public FeedbackInformation(string displayString, float yOffset, float duration) 
	{
		this.displayString = displayString;
		this.yOffset = yOffset;
		this.duration = duration;
	}
		
	public FeedbackInformation(string displayString, Color labelColor, float duration) 
	{
		this.displayString = displayString;
		this.labelColor = labelColor;
		this.duration = duration;
	}
	
	public FeedbackInformation(string displayString, Color labelColor, int fontSize, float duration) 
	{
		this.displayString = displayString;
		this.labelColor = labelColor;
		this.fontSize = fontSize;
		this.duration = duration;
	}
	
	public FeedbackInformation(string displayString, Color labelColor, int fontSize, float yOffset, float duration) {
		this.displayString = displayString;
		this.labelColor = labelColor;
		this.fontSize = fontSize;
		this.yOffset = yOffset;
		this.duration = duration;
		
	}
	
}
