using UnityEngine;
using System.Collections;

public enum Heading {
		None,
		North,
		South,
		East,
		West,
		NorthEast,
		NorthWest,
		SouthEast,
		SouthWest
}

public static class AngleTools  {

	// The angle between dirA and dirB around axis
	public static float AngleAroundAxis (Vector3 directionFrom, Vector3 directionTo, Vector3 axis) {
	   
	    // Find (positive) angle between A and B
	    float angle = Vector3.Angle (directionFrom, directionTo);
	   
	    // Return angle multiplied with 1 or -1
	    return angle * (Vector3.Dot (axis, Vector3.Cross (directionFrom, directionTo)) < 0 ? -1 : 1);
	}
	
	public static Heading HeadingTo(Collider collider, Transform originTransform) {
		
		float angleToCollider = AngleTools.AngleAroundAxis(collider.transform.position, originTransform.up, Vector3.forward);
		
		if(angleToCollider >= -22.5f && angleToCollider < 22.5f)
			return Heading.North;
		if(angleToCollider >= 22.5f && angleToCollider < 67.5f)
			return Heading.NorthEast;
		if(angleToCollider >= 67.5f && angleToCollider < 112.5f)
			return Heading.East;
		if(angleToCollider >= 112.5f && angleToCollider < 157.5f)
			return Heading.SouthEast;
		if((angleToCollider >= 157.5f && angleToCollider <= 180) || (angleToCollider >= -180 && angleToCollider < -157.5 ))
			return Heading.South;
		if(angleToCollider >= -157.5f && angleToCollider < -112.5f)
			return Heading.SouthWest;
		if(angleToCollider >= 112.5f && angleToCollider < 67.5f)
			return Heading.West;
		if(angleToCollider >= 67.5f && angleToCollider < -22.5f)
			return Heading.NorthWest;
		   
		return Heading.None;
	}
	
}
