using UnityEngine;
using System.Collections;

public class CollisionDamager : MonoBehaviour {

	public float damage = 1;
	
	void Start()
	{
		// For initialization.  NOTE: I put this here so i could enable or disable this in the editor
	}
	
	void OnCollisionEnter(Collision collision) 
	{
		
		if(!this.enabled) return;
		
		if(collision.gameObject.layer == this.gameObject.layer)
			return;
		
		Health collidedHealth = collision.collider.GetComponent<Health>();
		
		if(collidedHealth != null) {
			
			collidedHealth.OnDamage(damage);
				
		}
		
		
	}
	
}
