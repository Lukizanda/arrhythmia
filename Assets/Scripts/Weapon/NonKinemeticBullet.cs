using UnityEngine;
using System.Collections;

public class NonKinemeticBullet : MonoBehaviour {

	public float speed = 10.0f;
	public float lifeTime = 0.5f;
	public float damage = 1.0f;
	private float spawnTime = 0.0f;
	public Transform postDestructionPrefab;
	
	void Awake()
	{
		spawnTime = Time.time;
	}
	
	void OnSpawned()
	{
		spawnTime = Time.time; // + Random.value;
		rigidbody.AddForce(transform.up * speed, ForceMode.Impulse);
	}
	
	void OnDespawned()
	{
		rigidbody.velocity = Vector3.zero;
		rigidbody.angularVelocity = Vector3.zero;
	}
	
	void Update() {
		
		if(Time.time > spawnTime + lifeTime) {
			DestroySelf();
			//PoolManager.Pools["Projectile"].Despawn(this.transform);
		}
		
		
		
	}
	
	void OnCollisionEnter(Collision collision) 
	{
		
		Health colliderHealth = collision.gameObject.GetComponent<Health>();
				
		if(colliderHealth != null) 
		{
			colliderHealth.OnDamage(damage);
			
			DestroySelf();
		}
		
		
	}
	
	void DestroySelf()
	{
		// doing this, coz we need to destroy the object immediately, waiting for send message is too slow.
		
		if(postDestructionPrefab) {
			PoolManager.Pools["Explosion"].Spawn(postDestructionPrefab, new Vector3(transform.position.x, transform.position.y, 0), Quaternion.identity);
		}
		
		if(this.gameObject.active)
			PoolManager.Pools["Projectile"].Despawn(this.transform);
			
	}
	
}
