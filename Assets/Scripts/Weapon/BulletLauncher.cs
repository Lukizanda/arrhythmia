using UnityEngine;
using System.Collections;

public class BulletLauncher : MonoBehaviour {

	public float rateOfFire = 1f;
	public GameObject bulletPrefab;
	public Transform spawnPoint;
	
	public void LaunchProjectile()
	{
		// animate launcher
		//tweenScale.to = new Vector3(1.5f, 1.5f, 1f);
		//tweenScale.from = new Vector3(1f, 1f, 1f);
		//tweenScale.Reset();
		//tweenScale.Play(true);
		iTween.PunchScale(this.gameObject, Vector3.one * 1.5f, .5f);
		
		Vector3 randomDirection = new Vector3(0, 0, Random.Range(0, 360));
		
		PoolManager.Pools["Projectile"].Spawn(bulletPrefab.transform, spawnPoint.position, Quaternion.Euler(randomDirection));
	}
	
	IEnumerator BulletLaunchCoroutine()
	{	
		float timeRemaining = rateOfFire;
		
		while(true) {
			
			if(timeRemaining > 0) {
			
				timeRemaining -= Time.deltaTime;
			
			 } else {
				
				timeRemaining = rateOfFire;
				
				Vector3 randomDirection = Random.onUnitSphere;
				
				randomDirection = new Vector3(randomDirection.x, randomDirection.y, 0.0f);

				PoolManager.Pools["Projectile"].Spawn(bulletPrefab.transform, spawnPoint.position, Quaternion.Euler(randomDirection));
				
			}
			
			yield return null;
		}
		
		
	}
	
	
}
